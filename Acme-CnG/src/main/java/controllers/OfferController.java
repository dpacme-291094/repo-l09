
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CustomerService;
import services.OfferService;
import domain.Actor;
import domain.Administrator;
import domain.Customer;
import domain.Offer;

@Controller
@RequestMapping("/offer")
public class OfferController extends AbstractController {

	// Services ---------------------------------------
	@Autowired
	private OfferService	offerService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private CustomerService	customerService;


	// Constructors -----------------------------------
	public OfferController() {
		super();
	}

	// Listing -----------------------------------

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView viewList(@Valid final int offerId) {
		ModelAndView result;

		final Offer offer = this.offerService.findOne(offerId);
		boolean showBanPost = false, showBanComment = false;

		if (offer == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		else {

			final Actor a = this.actorService.findByPrincipal();
			if (a instanceof Administrator) {
				showBanPost = true;
				showBanComment = true;
			} else if (a instanceof Customer && a.equals(offer.getCustomer())) {
				showBanPost = true;
				showBanComment = true;
			}

			if (!offer.isBanned() || showBanPost) {
				result = new ModelAndView("offer/view");
				result.addObject("offer", offer);
				result.addObject("showBanComment", showBanComment);
			} else {
				final Customer c = this.customerService.findByPrincipal();
				final Collection<Offer> offerr = this.offerService.findByCustomer(c);

				result = new ModelAndView("offer/list");
				result.addObject("offer", offerr);
				result.addObject("message", "error.post.banned");
			}

			result.addObject("isnotcustomer", a.getId());
		}
		return result;
	}
}
