
package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.OfferService;
import controllers.AbstractController;
import domain.Offer;

@Controller
@RequestMapping("/offer/administrator")
public class OfferAdministratorController extends AbstractController {

	// Services ---------------------------------------
	@Autowired
	private OfferService	offerService;


	// Constructors -----------------------------------
	public OfferAdministratorController() {
		super();
	}

	// Listing -----------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		Collection<Offer> offer = null;

		offer = this.offerService.findAll();

		result = new ModelAndView("offer/administrator/list");
		result.addObject("offer", offer);

		return result;
	}
	@RequestMapping(value = "/listBan", method = RequestMethod.GET)
	public ModelAndView list(@Valid final Integer commentId) {
		ModelAndView result;
		final Collection<Offer> offers;
		offers = this.offerService.findOfferNotPassed();

		result = new ModelAndView("offer/listBan");
		result.addObject("offers", offers);

		return result;
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/listBan", method = RequestMethod.POST, params = "ban")
	public ModelAndView ban(@Valid Offer offer, final BindingResult binding) {
		ModelAndView result;
		final Collection<Offer> offers;
		offers = this.offerService.findOfferNotPassed();

		result = new ModelAndView("offer/listBan");

		if (binding.hasErrors()) {
			result.addObject("offer", offer);
			result.addObject("message", null);
		} else {
			offer = this.offerService.reconstruct(offer, binding);
			try {
				offer.setBanned(true);
				offer = this.offerService.ban(offer);
				result.addObject("offer", offer);
				result.addObject("message1", "configuration.commit.ok");
			} catch (final Throwable oops) {
				result.addObject("offers", offer);
				result.addObject("message", "configuration.commit.error");
			}
		}

		result.addObject("offers", offers);
		return result;
	}
}
