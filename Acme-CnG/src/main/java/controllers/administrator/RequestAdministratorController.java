
package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.RequestService;
import controllers.AbstractController;
import domain.Request;

@Controller
@RequestMapping("/request/administrator")
public class RequestAdministratorController extends AbstractController {

	// Services ---------------------------------------
	@Autowired
	private RequestService	requestService;


	// Constructors -----------------------------------
	public RequestAdministratorController() {
		super();
	}

	// Listing -----------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		final Collection<Request> requests = this.requestService.findAll();

		result = new ModelAndView("request/administrator/list");
		result.addObject("request", requests);

		return result;
	}

	@RequestMapping(value = "/listBan", method = RequestMethod.GET)
	public ModelAndView list(@Valid final Integer commentId) {
		ModelAndView result;
		final Collection<Request> requests;
		requests = this.requestService.findRequestNotPassed();

		result = new ModelAndView("request/listBan");
		result.addObject("requests", requests);

		return result;
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/listBan", method = RequestMethod.POST, params = "ban")
	public ModelAndView ban(@Valid Request request, final BindingResult binding) {
		ModelAndView result;
		final Collection<Request> requests;
		requests = this.requestService.findRequestNotPassed();

		result = new ModelAndView("request/listBan");

		if (binding.hasErrors()) {
			result.addObject("request", request);
			result.addObject("message", null);
		} else {
			request = this.requestService.reconstruct(request, binding);
			try {
				request.setBanned(true);
				request = this.requestService.save(request);
				result.addObject("request", request);
				result.addObject("message1", "configuration.commit.ok");
			} catch (final Throwable oops) {
				result.addObject("requests", request);
				result.addObject("message", "configuration.commit.error");
			}
		}

		result.addObject("requests", requests);
		return result;
	}
}
