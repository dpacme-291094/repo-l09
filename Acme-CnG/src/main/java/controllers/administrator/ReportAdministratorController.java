
package controllers.administrator;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CommentService;
import services.CustomerService;
import services.MessageService;
import services.PostService;
import controllers.AbstractController;
import domain.Actor;

@Controller
@RequestMapping("/report/administrator")
public class ReportAdministratorController extends AbstractController {

	// Services ---------------------------------------

	@Autowired
	private PostService		postService;

	@Autowired
	private CustomerService	customerService;

	@Autowired
	private CommentService	commentService;

	@Autowired
	private ActorService	actorService;

	@Autowired
	private MessageService	messageService;


	// Constructors -----------------------------------

	public ReportAdministratorController() {
		super();
	}

	// view -----------------------------------------
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		//C queries

		//Query 1
		final Collection<Object[]> ror = this.postService.ratioOfferRequest();
		//Query 2
		final Double opc = this.postService.offerPerCustomer();
		final Double rpc = this.postService.requestPerCustomer();
		//Query 3
		final Collection<Object[]> app = this.postService.applicationsPerPost();
		//Query 4
		final Collection<Object[]> cmaa = this.customerService.customerMoreApplicationAccepted();
		//Query 5
		final Collection<Object[]> cmad = this.customerService.customerMoreApplicationDenied();

		//B queries
		//Query 6
		final Collection<Double> cacr = this.commentService.calculateAvgCommentFromRequest();
		final Collection<Double> caco = this.commentService.calculateAvgCommentFromOffer();
		final Collection<Double> caca = this.commentService.calculateAvgCommentFromActor();

		//Query 7
		final Collection<Double> cacc = this.commentService.calculateAvgCommentFromCustomer();
		final Collection<Double> cacaa = this.commentService.calculateAvgCommentFromAdministrator();

		//Query 8
		final Collection<Actor> mtp = this.actorService.moreTenPercent();
		final Collection<Actor> ltp = this.actorService.lessTenPercent();

		//A queries

		result = new ModelAndView("report/dashboard");

		//C queries

		//Query 1
		result.addObject("ror", ror);
		//Query 2
		result.addObject("opc", opc);
		result.addObject("rpc", rpc);
		//Query 3
		result.addObject("app", app);
		//Query 4
		HashSet<Object[]> aux2 = new HashSet<Object[]>();
		int max = 0;
		for (final Object[] ao : cmaa)
			if (aux2.size() == 0) {
				aux2.add(ao);
				max = Integer.parseInt(ao[1].toString());
			} else if (max == Integer.parseInt(ao[1].toString()))
				aux2.add(ao);
			else
				break;
		result.addObject("cmaa", aux2);
		//Query 5
		aux2 = new HashSet<Object[]>();
		max = 0;
		for (final Object[] ao : cmad)
			if (aux2.size() == 0) {
				aux2.add(ao);
				max = Integer.parseInt(ao[1].toString());
			} else if (max == Integer.parseInt(ao[1].toString()))
				aux2.add(ao);
			else
				break;
		result.addObject("cmad", aux2);

		//B queries

		//Query 6
		result.addObject("cacr", cacr);
		result.addObject("caco", caco);
		result.addObject("caca", caca);

		//Query 7
		result.addObject("cacc", cacc);
		result.addObject("cacaa", cacaa);

		//Query 8
		result.addObject("mtp", mtp);
		result.addObject("ltp", ltp);

		//A queries
		final Collection<Object[]> consultaA1 = this.messageService.minAvgMaxMessageSendPerActor();
		final Collection<Object[]> consultaA2 = this.messageService.minAvgMaxMessageReceivedPerActor();
		final Collection<Object[]> consultaA3 = this.messageService.actorsSendMaxMessages();
		final Collection<Object[]> consultaA4 = this.messageService.actorsMaxMessages();

		result.addObject("consultaA1", consultaA1);
		result.addObject("consultaA2", consultaA2);
		result.addObject("consultaA3", consultaA3);
		result.addObject("consultaA4", consultaA4);
		return result;
	}
}
