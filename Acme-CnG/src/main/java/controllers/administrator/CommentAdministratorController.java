
package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CommentService;
import controllers.AbstractController;
import domain.Comment;

@Controller
@RequestMapping("/comment/administrator")
public class CommentAdministratorController extends AbstractController {

	// Services ---------------------------------------
	@Autowired
	private CommentService	commentService;


	// Constructors -----------------------------------
	public CommentAdministratorController() {
		super();
	}

	// Listing -----------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@Valid final Integer commentId) {
		ModelAndView result;
		final Collection<Comment> comments;
		comments = this.commentService.findAll();

		result = new ModelAndView("comment/list");
		result.addObject("comments", comments);

		return result;
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "ban")
	public ModelAndView ban(@Valid Comment comment, final BindingResult binding) {
		ModelAndView result;
		final Collection<Comment> comments;
		comments = this.commentService.findAll();

		result = new ModelAndView("comment/list");

		if (binding.hasErrors()) {
			result.addObject("comment", comment);
			result.addObject("message", null);
		} else {
			comment = this.commentService.reconstruct(comment, binding);
			try {
				comment.setBanned(true);
				comment = this.commentService.save(comment);
				result.addObject("comment", comment);
				result.addObject("message1", "configuration.commit.ok");
			} catch (final Throwable oops) {
				result.addObject("comment", comment);
				result.addObject("message", "configuration.commit.error");
			}
		}

		result.addObject("comments", comments);
		return result;
	}
}
