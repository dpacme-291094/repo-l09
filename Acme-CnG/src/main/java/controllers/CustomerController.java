
package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CustomerService;
import domain.Actor;
import domain.Customer;
import forms.FormCustomer;

@Controller
@RequestMapping("/customer")
public class CustomerController extends AbstractController {

	@Autowired
	private CustomerService	customerService;

	@Autowired
	private ActorService	actorService;


	public CustomerController() {
		super();
	}

	// Create
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView create() {
		final FormCustomer formCustomer = this.customerService.createForm();
		final ModelAndView result = new ModelAndView("customer/register");

		result.addObject("formCustomer", formCustomer);
		return result;
	}

	protected ModelAndView createEditModelAndView(final FormCustomer customer) {

		ModelAndView result;

		result = this.createEditModelAndView(customer, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final FormCustomer customer, final String message) {
		ModelAndView result;

		result = new ModelAndView("customer/register");
		result.addObject("formCustomer", customer);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "register")
	public ModelAndView register(@ModelAttribute("formCustomer") final FormCustomer formCustomer, final BindingResult binding, final boolean terms, final String repassword) {

		ModelAndView result;
		String redirectView;

		final Customer customer = this.customerService.reconstruct(formCustomer, binding);

		if (binding.hasErrors())
			result = this.createEditModelAndView(formCustomer);
		else
			try {
				final String username = customer.getUserAccount().getUsername();

				if (this.actorService.existsUsername(username))
					result = this.createEditModelAndView(formCustomer, "actor.commit.error");
				else if (terms == false)
					result = this.createEditModelAndView(formCustomer, "actor.terms");
				else if (!repassword.equals(customer.getUserAccount().getPassword()))
					result = this.createEditModelAndView(formCustomer, "actor.pass");
				else {

					this.customerService.save(customer);
					redirectView = "redirect:../security/login.do";
					result = new ModelAndView(redirectView);
				}

			} catch (final Throwable oops) {

				result = this.createEditModelAndView(formCustomer, "actor.commit.error2");
			}
		return result;
	}

	// Profile -----------------------------------
	@RequestMapping(value = "/profileForOwner", method = RequestMethod.GET)
	public ModelAndView profileForOwner() {
		ModelAndView result;
		final Boolean hiddenComment = false;

		final Customer customer = this.customerService.findByPrincipal();

		result = new ModelAndView("customer/profile");
		result.addObject("customer", customer);
		result.addObject("hiddenComment", hiddenComment);
		result.addObject("isOwner", true);

		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(@Valid final int customerId) {
		ModelAndView result;
		Boolean hiddenComment = true;

		final Customer customer = (Customer) this.customerService.findOne(customerId);
		if (customer == null)
			throw new RuntimeException("Oops! An exception was thrown.");
		else {

			hiddenComment = this.checkHiddenComment(customer);
			result = new ModelAndView("customer/profile");
			result.addObject("customer", customer);
			result.addObject("hiddenComment", hiddenComment);
			result.addObject("isOwner", false);

		}
		return result;
	}

	private Boolean checkHiddenComment(final Customer customer) {
		Boolean hiddenComment = true;
		final Actor a = this.actorService.findByPrincipal();

		if (customer.equals(a))
			hiddenComment = false;

		return hiddenComment;
	}
}
