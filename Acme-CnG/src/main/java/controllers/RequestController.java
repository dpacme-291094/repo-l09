
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CustomerService;
import services.RequestService;
import domain.Actor;
import domain.Administrator;
import domain.Customer;
import domain.Request;

@Controller
@RequestMapping("/request")
public class RequestController extends AbstractController {

	// Services ---------------------------------------
	@Autowired
	private RequestService	requestService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private CustomerService	customerService;


	// Constructors -----------------------------------
	public RequestController() {
		super();
	}

	// Listing -----------------------------------

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView viewList(@Valid final int requestId) {
		ModelAndView result;

		final Request request = this.requestService.findOne(requestId);
		boolean showBanPost = false;
		boolean showBanComment = false;
		if (request == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		else {
			final Actor a = this.actorService.findByPrincipal();
			if (a instanceof Administrator) {
				showBanPost = true;
				showBanComment = true;
			} else if (a instanceof Customer && a.equals(request.getCustomer())) {
				showBanPost = true;
				showBanComment = true;
			}

			if (!request.isBanned() || showBanPost) {
				result = new ModelAndView("request/view");
				result.addObject("request", request);
				result.addObject("showBanComment", showBanComment);
			} else {
				final Customer c = this.customerService.findByPrincipal();
				final Collection<Request> requestt = this.requestService.findByCustomer(c);

				result = new ModelAndView("request/list");
				result.addObject("request", requestt);
			}

			result.addObject("isnotcustomer", a.getId());
		}
		return result;
	}
}
