
package controllers.customer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ApplicationService;
import services.CustomerService;
import services.RequestService;
import controllers.AbstractController;
import domain.Coordinates;
import domain.Customer;
import domain.Request;
import forms.FormRequest;

@Controller
@RequestMapping("/request/customer")
public class RequestCustomerController extends AbstractController {

	// Services ---------------------------------------
	@Autowired
	private RequestService		requestService;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private ApplicationService	applicationService;


	// Constructors -----------------------------------
	public RequestCustomerController() {
		super();
	}

	// Listing -----------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		final Customer c = this.customerService.findByPrincipal();
		final Collection<Request> request = this.requestService.findByCustomer(c);

		result = new ModelAndView("request/list");
		result.addObject("request", request);

		return result;
	}
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView viewList(@Valid final int requestId) {
		ModelAndView result;
		boolean showBanComment = false;

		final Request request = this.requestService.findOne(requestId);

		result = new ModelAndView("request/view");

		result.addObject("request", request);
		final Customer c = this.customerService.findByPrincipal();
		result.addObject("isnotcustomer", c.getId());
		if (request.getCustomer().equals(c))
			showBanComment = true;
		result.addObject("showBanComment", showBanComment);

		return result;
	}
	@RequestMapping(value = "/view", method = RequestMethod.POST, params = "accept")
	public ModelAndView accepted(final Request request) {

		ModelAndView result = null;
		String redirectView;

		try {

			this.requestService.accept(request);
			redirectView = "redirect:../../application/customer/privateList.do";
			result = new ModelAndView(redirectView);

		} catch (final Throwable oops) {

		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		//final Request request = this.requestService.create();
		final FormRequest formRequest = new FormRequest();

		formRequest.setCoordinatesDestination(new Coordinates());
		formRequest.setCoordinatesOrigin(new Coordinates());

		result = new ModelAndView("request/create");
		result.addObject("FormRequest", formRequest);
		final Collection<String> places = Arrays.asList("1", "2", "3", "4");
		result.addObject("places", places);

		return result;
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("FormRequest") final FormRequest formRequest, final BindingResult binding) {
		ModelAndView result;
		final Date ahora = new Date();

		final Request request = this.requestService.reconstruct(formRequest, binding);
		final Coordinates origin = request.getCoordinatesOrigin();
		final Coordinates dest = request.getCoordinatesDestination();
		if (binding.hasErrors())
			result = this.createEditModelAndView(request);
		else if (ahora.after(request.getMoment()))
			result = this.createEditModelAndView(request, "post.error.date");
		else if ((origin.getLatitude() == null && origin.getLongitude() != null) || (origin.getLatitude() != null && origin.getLongitude() == null))
			result = this.createEditModelAndView(request, "post.error.origin");
		else if ((dest.getLatitude() == null && dest.getLongitude() != null) || (dest.getLatitude() != null && dest.getLongitude() == null))
			result = this.createEditModelAndView(request, "post.error.dest");
		else
			try {
				this.requestService.save(request);

				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				System.out.println(oops.toString());
				result = this.createEditModelAndView(request, "request.commit.error");
			}
		return result;
	}
	private ModelAndView createEditModelAndView(final Request request) {
		return this.createEditModelAndView(request, null);
	}

	private ModelAndView createEditModelAndView(final Request request, final String message) {
		ModelAndView result;

		result = new ModelAndView("request/create");
		result.addObject("request", request);
		result.addObject("message", message);
		final Collection<String> places = Arrays.asList("1", "2", "3", "4");
		result.addObject("places", places);
		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView search(@RequestParam final String keyWord) {
		ModelAndView result;
		final Collection<Request> requests;

		final Customer customer = this.customerService.findByPrincipal();
		requests = this.requestService.findRequestByTitleDescritionOrPlacesNotBanned(keyWord);

		result = new ModelAndView("request/search");
		result.addObject("requests", requests);
		return result;
	}
}
