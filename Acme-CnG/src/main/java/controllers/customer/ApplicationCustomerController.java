
package controllers.customer;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ApplicationService;
import services.CustomerService;
import controllers.AbstractController;
import domain.Application;
import domain.Customer;

@Controller
@RequestMapping("application/customer")
public class ApplicationCustomerController extends AbstractController {

	@Autowired
	private ApplicationService	applicationService;

	@Autowired
	private CustomerService		customerService;


	public ApplicationCustomerController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		final Customer c = this.customerService.findByPrincipal();
		final Collection<Application> applications = this.applicationService.findApplicationByCustomer(c.getId());

		result = new ModelAndView("application/list");
		result.addObject("applications", applications);

		return result;

	}
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "accepted")
	public ModelAndView accepted(final int application) {

		ModelAndView result = null;
		String redirectView;
		final Application a = this.applicationService.findOne(application);
		if (a.getPost().getPlaces() == 0) {
			result = this.list();
			result.addObject("message", "app.noseats");
		} else
			try {

				this.applicationService.accept(a);
				redirectView = "redirect:list.do";
				result = new ModelAndView(redirectView);

			} catch (final Throwable oops) {
				redirectView = "redirect:list.do";
			}
		return result;

	}
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "denied")
	public ModelAndView denied(final int application) {

		ModelAndView result = null;
		String redirectView = null;

		try {
			final Application a = this.applicationService.findOne(application);

			this.applicationService.deny(a);
			redirectView = "redirect:list.do";
			result = new ModelAndView(redirectView);

		} catch (final Throwable oops) {

		}

		return result;
	}
	@RequestMapping(value = "/privateList", method = RequestMethod.GET)
	public ModelAndView privateList() {

		ModelAndView result;
		final Customer c = this.customerService.findByPrincipal();
		final Collection<Application> applications = c.getApplications();

		result = new ModelAndView("application/privateList");
		result.addObject("applications", applications);

		return result;

	}

}
