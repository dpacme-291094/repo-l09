
package controllers.customer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ApplicationService;
import services.CustomerService;
import services.OfferService;
import controllers.AbstractController;
import domain.Coordinates;
import domain.Customer;
import domain.Offer;
import forms.FormOffer;

@Controller
@RequestMapping("/offer/customer")
public class OfferCustomerController extends AbstractController {

	// Services ---------------------------------------
	@Autowired
	private OfferService		offerService;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private ApplicationService	applicationService;


	// Constructors -----------------------------------
	public OfferCustomerController() {
		super();
	}

	// Listing -----------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		final Customer c = this.customerService.findByPrincipal();
		final Collection<Offer> offer = this.offerService.findByCustomer(c);

		result = new ModelAndView("offer/list");
		result.addObject("offer", offer);

		return result;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView viewList(@Valid final int offerId) {
		ModelAndView result;
		boolean showBanComment = false;

		final Offer offer = this.offerService.findOne(offerId);

		result = new ModelAndView("offer/view");

		result.addObject("offer", offer);
		final Customer c = this.customerService.findByPrincipal();
		result.addObject("isnotcustomer", c.getId());
		if (offer.getCustomer().equals(c))
			showBanComment = true;
		result.addObject("showBanComment", showBanComment);

		return result;
	}
	@RequestMapping(value = "/view", method = RequestMethod.POST, params = "accept")
	public ModelAndView accepted(final Offer offer) {

		ModelAndView result = null;
		String redirectView;

		try {

			this.offerService.accept(offer);
			redirectView = "redirect:../../application/customer/privateList.do";
			result = new ModelAndView(redirectView);

		} catch (final Throwable oops) {

		}

		return result;
	}
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		//final Offer offer = this.offerService.create();
		final FormOffer formOffer = new FormOffer();
		result = new ModelAndView("offer/create");
		formOffer.setCoordinatesDestination(new Coordinates());
		formOffer.setCoordinatesOrigin(new Coordinates());

		result.addObject("FormOffer", formOffer);
		final Collection<String> places = Arrays.asList("1", "2", "3", "4");
		result.addObject("places", places);
		return result;
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("FormOffer") final FormOffer formOffer, final BindingResult binding) {
		ModelAndView result;
		final Date ahora = new Date();
		final Offer offer = this.offerService.reconstruct(formOffer, binding);
		final Coordinates origin = offer.getCoordinatesOrigin();
		final Coordinates dest = offer.getCoordinatesDestination();
		if (binding.hasErrors())
			result = this.createEditModelAndView(offer);
		else if (ahora.after(offer.getMoment()))
			result = this.createEditModelAndView(offer, "post.error.date");
		else if ((origin.getLatitude() == null && origin.getLongitude() != null) || (origin.getLatitude() != null && origin.getLongitude() == null))
			result = this.createEditModelAndView(offer, "post.error.origin");
		else if ((dest.getLatitude() == null && dest.getLongitude() != null) || (dest.getLatitude() != null && dest.getLongitude() == null))
			result = this.createEditModelAndView(offer, "post.error.dest");
		else
			try {
				this.offerService.save2(offer);

				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				System.out.println(oops.toString());
				result = this.createEditModelAndView(offer, "offer.commit.error");
			}
		return result;
	}
	private ModelAndView createEditModelAndView(final Offer offer) {
		return this.createEditModelAndView(offer, null);
	}

	private ModelAndView createEditModelAndView(final Offer offer, final String message) {
		ModelAndView result;

		result = new ModelAndView("offer/create");
		result.addObject("offer", offer);
		result.addObject("message", message);
		final Collection<String> places = Arrays.asList("1", "2", "3", "4");
		result.addObject("places", places);
		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView search(@RequestParam final String keyWord) {
		ModelAndView result;
		final Collection<Offer> offers;

		final Customer customer = this.customerService.findByPrincipal();
		offers = this.offerService.findOfferByTitleDescritionOrPlacesNotBanned(keyWord);

		result = new ModelAndView("offer/search");
		result.addObject("offers", offers);
		return result;
	}
}
