
package controllers;

import java.util.Arrays;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AdministratorService;
import services.CommentService;
import services.OfferService;
import services.PostService;
import services.RequestService;
import domain.Actor;
import domain.Administrator;
import domain.Comment;
import domain.Offer;
import domain.Post;
import domain.Request;

@Controller
@RequestMapping(value = {
	"/comment/actor"
})
public class CommentController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private CommentService			commentService;
	@Autowired
	private PostService				postService;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private RequestService			requestService;
	@Autowired
	private OfferService			offerService;
	@Autowired
	private AdministratorService	administratorService;


	// Constructors -----------------------------------------------------------

	public CommentController() {
		super();
	}

	// Creation ----------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final Integer rec, @RequestParam final char type) {

		final Comment comment = this.commentService.create(rec, type);

		final ModelAndView result = this.createEditModelAndView(comment);

		return result;

	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Comment comment, final BindingResult binding) {
		ModelAndView result = null;
		final Actor recipientActor;
		final Post recipientPost;

		if (binding.hasErrors())
			result = this.createEditModelAndView(comment);
		else
			try {
				Comment savedComment;
				final Actor authorActor = this.actorService.findByPrincipal();
				final String[] parts = comment.getRecipient().split(",");
				final String id = parts[0];
				final String commentableEntity = parts[1];

				switch (commentableEntity) {

				case "A":
					final Administrator administrator = this.administratorService.findAdministrator();
					recipientActor = this.actorService.findOne(Integer.valueOf(id));

					savedComment = this.commentService.save(comment);

					recipientActor.addComment(savedComment);
					this.actorService.save(recipientActor);

					if (recipientActor.equals(administrator))
						result = new ModelAndView("redirect: ../../../../administrator/profileAdmin.do");
					else
						result = new ModelAndView("redirect: ../../../../customer/profile.do?customerId=" + recipientActor.getId());

					break;
				case "P":
					recipientPost = this.postService.findOne(Integer.valueOf(id));
					savedComment = this.commentService.save(comment);

					recipientPost.addComment(savedComment);

					if (recipientPost instanceof Request) {
						this.requestService.save((Request) recipientPost);
						result = new ModelAndView("redirect: ../../../../request/view.do?requestId=" + recipientPost.getId());
					} else if (recipientPost instanceof Offer) {
						this.offerService.edit((Offer) recipientPost);
						result = new ModelAndView("redirect: ../../../../offer/view.do?offerId=" + recipientPost.getId());
					}
					break;

				default:
					result = new ModelAndView("/");
					break;
				}

			} catch (final Throwable oops) {
				result = this.createEditModelAndView(comment, "comment.commit.error");
			}
		return result;
	}
	// Ancillary Methods
	// ----------------------------------------------------------------

	protected ModelAndView createEditModelAndView(final Comment comment) {
		final ModelAndView result = this.createEditModelAndView(comment, null);
		return result;
	}

	// Creation ----------------------------------------------------------------

	private ModelAndView createEditModelAndView(final Comment comment, final String message) {
		final ModelAndView result = new ModelAndView("comment/edit");

		final Collection<String> stars = Arrays.asList("0", "1", "2", "3", "4", "5");

		result.addObject("comment", comment);
		result.addObject("stars", stars);
		result.addObject("message", message);
		return result;
	}

}
