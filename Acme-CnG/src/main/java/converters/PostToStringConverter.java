
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Post;

@Component
@Transactional
public class PostToStringConverter implements Converter<Post, String> {

	@Override
	public String convert(final Post post) {
		Assert.notNull(post);
		final String result = String.valueOf(post.getId());

		return result;
	}

}
