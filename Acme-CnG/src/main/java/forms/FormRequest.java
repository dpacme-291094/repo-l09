
package forms;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

import domain.Coordinates;

public class FormRequest {

	private String		title;
	private String		description;
	private String		addressOrigin;
	private Coordinates	coordinatesOrigin;
	private String		addressDestination;
	private Coordinates	coordinatesDestination;
	private Date		moment;
	private int			places;


	@Range(min = 0, max = 4)
	public int getPlaces() {
		return this.places;
	}

	public void setPlaces(final int places) {
		this.places = places;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getAddressOrigin() {
		return this.addressOrigin;
	}

	public void setAddressOrigin(final String addressOrigin) {
		this.addressOrigin = addressOrigin;
	}

	@AttributeOverrides({
		@AttributeOverride(name = "latitude", column = @Column(name = "latitudeOrigin")), @AttributeOverride(name = "longitude", column = @Column(name = "longitudeOrigin"))
	})
	@Valid
	public Coordinates getCoordinatesOrigin() {
		return this.coordinatesOrigin;
	}

	public void setCoordinatesOrigin(final Coordinates coordinatesOrigin) {
		this.coordinatesOrigin = coordinatesOrigin;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getAddressDestination() {
		return this.addressDestination;
	}

	public void setAddressDestination(final String addressDestination) {
		this.addressDestination = addressDestination;
	}

	@AttributeOverrides({
		@AttributeOverride(name = "latitude", column = @Column(name = "latitudeDest")), @AttributeOverride(name = "longitude", column = @Column(name = "longitudeDest"))
	})
	@Valid
	public Coordinates getCoordinatesDestination() {
		return this.coordinatesDestination;
	}

	public void setCoordinatesDestination(final Coordinates coordinatesDestination) {
		this.coordinatesDestination = coordinatesDestination;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@NotNull
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}
}
