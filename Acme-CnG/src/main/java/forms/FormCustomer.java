
package forms;

import java.util.Collection;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

import security.UserAccount;
import domain.Application;
import domain.Comment;
import domain.Message;
import domain.Post;

public class FormCustomer {

	private int						id;
	private int						version;
	private Collection<Post>		posts;
	private Collection<Application>	applications;
	private UserAccount				userAccount;
	private Collection<Message>		messagesIncoming;
	private Collection<Comment>		comments;


	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	public Collection<Message> getMessagesIncoming() {
		return this.messagesIncoming;
	}

	public void setMessagesIncoming(final Collection<Message> messagesIncoming) {
		this.messagesIncoming = messagesIncoming;
	}

	public Collection<Message> getMessagesOutgoing() {
		return this.messagesOutgoing;
	}

	public void setMessagesOutgoing(final Collection<Message> messagesOutgoing) {
		this.messagesOutgoing = messagesOutgoing;
	}


	private Collection<Message>	messagesOutgoing;

	private String				name;
	private String				surname;
	private String				email;
	private String				phone;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	@Email
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@NotBlank
	@Pattern(regexp = "^((([+])([0-9]{1,3})([ ])?)?(([0-9]{1})([0-9]{1})([0-9]{1}))?([ ])?([a-zA-Z0-9- ]{4,}))?$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public int getId() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	public void setUserAccount(final UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	public Collection<Post> getPosts() {
		return this.posts;
	}

	public void setPosts(final Collection<Post> posts) {
		this.posts = posts;
	}

	public Collection<Application> getApplications() {
		return this.applications;
	}

	public void setApplications(final Collection<Application> applications) {
		this.applications = applications;
	}

}
