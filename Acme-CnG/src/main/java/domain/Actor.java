
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

import security.UserAccount;

@Entity
@Access(AccessType.PROPERTY)
@Table(uniqueConstraints = {
	@UniqueConstraint(columnNames = "email")
})
public abstract class Actor extends DomainEntity implements Commentable {

	// Constructors -----------------------------------------------------------

	public Actor() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String				name;
	private String				surname;
	private String				email;
	private String				phone;
	private Collection<Comment>	comments;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	@Email
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@NotBlank
	@Pattern(regexp = "^((([+])([0-9]{1,3})([ ])?)?(([0-9]{1})([0-9]{1})([0-9]{1}))?([ ])?([a-zA-Z0-9- ]{4,}))?$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	@Override
	@ElementCollection
	public Collection<Comment> getComments() {

		return this.comments;
	}

	@Override
	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public void addComment(final Comment comment) {
		this.comments.add(comment);
	}


	// Relationships ----------------------------------------------------------

	private UserAccount			userAccount;
	private Collection<Message>	messagesIncoming;
	private Collection<Message>	messagesOutgoing;


	@NotNull
	@Valid
	@OneToOne(cascade = CascadeType.ALL, optional = false)
	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	public void setUserAccount(final UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "actorRecipient")
	public Collection<Message> getMessagesIncoming() {
		return this.messagesIncoming;
	}

	public void setMessagesIncoming(final Collection<Message> messagesIncoming) {
		this.messagesIncoming = messagesIncoming;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "actorSender")
	public Collection<Message> getMessagesOutgoing() {
		return this.messagesOutgoing;
	}

	public void setMessagesOutgoing(final Collection<Message> messagesOutgoing) {
		this.messagesOutgoing = messagesOutgoing;
	}

}
