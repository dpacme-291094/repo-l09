
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {
	@Index(columnList = "moment, places, banned, title, description, addressOrigin, addressDestination")
})
public class Offer extends Post {

	// Constructors -----------------------------------------------------------

	public Offer() {
		super();
	}

	// Attributes -------------------------------------------------------------

}
