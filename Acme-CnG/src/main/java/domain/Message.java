
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Message extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Message() {
		super();
	}


	// Attributes -------------------------------------------------------------
	private String	sender;
	private String	recipient;
	private Date	moment;
	private String	title;
	private String	text;
	private String	attachments;


	@Email
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSender() {
		return this.sender;
	}

	public void setSender(final String sender) {
		this.sender = sender;
	}

	@Email
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getRecipient() {
		return this.recipient;
	}

	public void setRecipient(final String recipient) {
		this.recipient = recipient;
	}

	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public String getAttachments() {
		return this.attachments;
	}

	public void setAttachments(final String attachments) {
		this.attachments = attachments;
	}


	// Relationships -------------------------------------------------------
	private Actor	actorSender;
	private Actor	actorRecipient;


	@Valid
	@ManyToOne(optional = true)
	public Actor getActorSender() {
		return this.actorSender;
	}

	public void setActorSender(final Actor actorSender) {
		this.actorSender = actorSender;
	}

	@Valid
	@ManyToOne(optional = true)
	public Actor getActorRecipient() {
		return this.actorRecipient;
	}

	public void setActorRecipient(final Actor actorRecipient) {
		this.actorRecipient = actorRecipient;
	}

}
