
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Post extends DomainEntity implements Commentable {

	// Constructors -----------------------------------------------------------

	public Post() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String				title;
	private String				description;
	private String				addressOrigin;
	private Coordinates			coordinatesOrigin;
	private String				addressDestination;
	private Coordinates			coordinatesDestination;
	private Date				moment;
	private Collection<Comment>	comments;
	private int					places;
	private boolean				banned;


	@Range(min = 0, max = 4)
	public int getPlaces() {
		return this.places;
	}

	public void setPlaces(final int places) {
		this.places = places;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotBlank
	public String getAddressOrigin() {
		return this.addressOrigin;
	}

	public void setAddressOrigin(final String addressOrigin) {
		this.addressOrigin = addressOrigin;
	}
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotBlank
	public String getAddressDestination() {
		return this.addressDestination;
	}

	public void setAddressDestination(final String addressDestination) {
		this.addressDestination = addressDestination;
	}

	@AttributeOverrides({
		@AttributeOverride(name = "latitude", column = @Column(name = "latitudeOrigin")), @AttributeOverride(name = "longitude", column = @Column(name = "longitudeOrigin"))
	})
	@Valid
	public Coordinates getCoordinatesOrigin() {
		return this.coordinatesOrigin;
	}

	public void setCoordinatesOrigin(final Coordinates coordinatesOrigin) {
		this.coordinatesOrigin = coordinatesOrigin;
	}

	@AttributeOverrides({
		@AttributeOverride(name = "latitude", column = @Column(name = "latitudeDest")), @AttributeOverride(name = "longitude", column = @Column(name = "longitudeDest"))
	})
	@Valid
	public Coordinates getCoordinatesDestination() {
		return this.coordinatesDestination;
	}

	public void setCoordinatesDestination(final Coordinates coordinatesDestination) {
		this.coordinatesDestination = coordinatesDestination;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@NotNull
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	public boolean isBanned() {
		return this.banned;
	}

	public void setBanned(final boolean banned) {
		this.banned = banned;
	}

	@Override
	@ElementCollection
	public Collection<Comment> getComments() {

		return this.comments;
	}

	@Override
	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public void addComment(final Comment comment) {
		this.comments.add(comment);
	}


	// Relationships ----------------------------------------------------------

	private Customer				customer;
	private Collection<Application>	applications;


	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(final Customer customer) {
		this.customer = customer;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "post")
	public Collection<Application> getApplications() {
		return this.applications;
	}

	public void setApplications(final Collection<Application> applications) {
		this.applications = applications;
	}

}
