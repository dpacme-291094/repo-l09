package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Customer extends Actor {

	// Constructors -----------------------------------------------------------

	public Customer() {
		super();
	}

	// Relationships
	// -------------------------------------------------------------

	private Collection<Post> posts;
	private Collection<Application> applications;

	@Valid
	@NotNull
	@OneToMany(mappedBy = "customer")
	public Collection<Post> getPosts() {
		return this.posts;
	}

	public void setPosts(Collection<Post> posts) {
		this.posts = posts;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "customer")
	public Collection<Application> getApplications() {
		return applications;
	}

	public void setApplications(Collection<Application> applications) {
		this.applications = applications;
	}

}
