
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ApplicationRepository;
import domain.Application;
import domain.Customer;
import domain.Post;

@Service
@Transactional
public class ApplicationService {

	@Autowired
	private ApplicationRepository	applicationRepository;
	@Autowired
	private CustomerService			customerService;
	@Autowired
	private OfferService			offerService;
	@Autowired
	private RequestService			requestService;
	@Autowired
	private PostService				postService;


	public ApplicationService() {
		super();
	}

	public Application create(final Post p) {
		final Application a = new Application();

		a.setStatus("PENDING");
		a.setMoment(p.getMoment());
		a.setCustomer(this.customerService.findByPrincipal());
		a.setPost(p);

		return a;
	}

	public Application save(final Application a) {
		Assert.notNull(a);
		return this.applicationRepository.save(a);
	}

	public void delete(final int id) {
		this.applicationRepository.delete(id);
	}

	public Application findOne(final int id) {
		return this.applicationRepository.findOne(id);
	}

	public Collection<Application> findByCustomer(final int id) {
		return this.applicationRepository.findByCustomer(id);
	}
	public Collection<Application> findApplicationByCustomer(final int id) {
		return this.applicationRepository.findApplicationByCustomer(id);
	}

	public void accept(final Application a) {
		this.checkPrincipal(a.getPost().getCustomer());
		Assert.isTrue(a.getMoment().after(new Date()));

		final Post a2 = a.getPost();

		a2.setPlaces(a2.getPlaces() - 1);
		this.postService.save(a2);

		final Customer c = a.getCustomer();
		final Collection<Application> ca = c.getApplications();
		ca.remove(a);
		c.setApplications(ca);
		this.customerService.edit(c);
		a.setStatus("ACCEPTED");
		this.applicationRepository.save(a);
		ca.add(a);
		c.setApplications(ca);
		this.customerService.edit(c);

	}
	public void deny(final Application a) {
		this.checkPrincipal(a.getPost().getCustomer());
		Assert.isTrue(a.getMoment().after(new Date()));
		final Customer c = a.getCustomer();
		final Collection<Application> ca = c.getApplications();
		ca.remove(a);
		c.setApplications(ca);
		this.customerService.edit(c);
		a.setStatus("DENIED");
		this.applicationRepository.save(a);
		ca.add(a);
		c.setApplications(ca);
		this.customerService.edit(c);

	}

	public void checkPrincipal(final Customer c) {
		final Customer a = this.customerService.findByPrincipal();
		Assert.isTrue(a.equals(c));
	}

	public void flush() {
		this.applicationRepository.flush();
	}

	public Collection<Application> findAll() {
		// TODO Auto-generated method stub
		return this.applicationRepository.findAll();
	}
}
