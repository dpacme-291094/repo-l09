
package services;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.RequestRepository;
import security.Authority;
import domain.Actor;
import domain.Application;
import domain.Comment;
import domain.Customer;
import domain.Request;
import forms.FormRequest;

@Transactional
@Service
public class RequestService {

	// Managed repository -------------------------------
	@Autowired
	private RequestRepository	requestRepository;
	@Autowired
	private Validator			validator;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private ApplicationService	applicationService;
	@Autowired
	private ActorService		actorService;


	// Suporting services --------------------------------

	// Constructor --------------------------------------
	public RequestService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Request create() {
		final Request result = new Request();
		result.setBanned(false);
		result.setCustomer(this.customerService.findByPrincipal());
		result.setApplications(new HashSet<Application>());
		return result;
	}

	public Request save(final Request r) {
		Assert.notNull(r);
		final Request savedRequest = this.requestRepository.save(r);
		return savedRequest;
	}

	public Request reconstruct(final Request request, final BindingResult binding) {
		Request result;

		if (request.getId() == 0)
			result = request;
		else {
			result = this.requestRepository.findOne(request.getId());

			result.setTitle(request.getTitle());
			result.setAddressDestination(request.getAddressDestination());
			result.setAddressOrigin(request.getAddressOrigin());
			result.setApplications(request.getApplications());
			result.setBanned(request.isBanned());
			result.setDescription(request.getDescription());
			result.setMoment(request.getMoment());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public Request reconstruct(final FormRequest formRequest, final BindingResult binding) {
		final Request result = new Request();

		result.setTitle(formRequest.getTitle());
		result.setDescription(formRequest.getDescription());
		result.setAddressOrigin(formRequest.getDescription());
		result.setCoordinatesOrigin(formRequest.getCoordinatesOrigin());
		result.setAddressDestination(formRequest.getAddressDestination());
		result.setCoordinatesDestination(formRequest.getCoordinatesDestination());
		result.setMoment(formRequest.getMoment());
		result.setComments(new HashSet<Comment>());
		result.setApplications(new HashSet<Application>());
		result.setCustomer(this.customerService.findByPrincipal());
		result.setPlaces(formRequest.getPlaces());

		this.validator.validate(result, binding);
		return result;
	}

	public Collection<Object[]> calculateAvgRequest() {
		return this.requestRepository.calculateAvgRequest();
	}

	public Collection<Request> findByCustomerAndNotBanned(final Customer id) {
		return this.requestRepository.findByCustomerAndNotBanned(id);
	}

	public Collection<Request> findAll() {
		return this.requestRepository.findAll();
	}

	public Collection<Request> findAllNotBannedPending() {
		return this.requestRepository.findAllNotBannedPending();
	}

	public Request findOne(final int requestId) {

		return this.requestRepository.findOne(requestId);
	}
	public void accept(final Request o) {
		this.checkPrincipal();
		final Customer c = this.customerService.findByPrincipal();
		final Application a = this.applicationService.create(o);
		this.applicationService.save(a);
		final Collection<Application> ca = c.getApplications();
		ca.add(a);
		c.setApplications(ca);
		this.customerService.edit(c);

	}
	public void checkPrincipal() {
		final Actor a = this.actorService.findByPrincipal();
		for (final Authority b : a.getUserAccount().getAuthorities())
			Assert.isTrue(b.getAuthority().equals("CUSTOMER"));

	}

	public Collection<Request> findByCustomer(final Customer id) {
		return this.requestRepository.findByCustomer(id);
	}

	public void flush() {
		this.requestRepository.flush();
	}

	public Collection<Request> findRequestByTitleDescritionOrPlacesNotBanned(final String keyWord) {
		return this.requestRepository.findRequestByTitleDescritionOrPlacesNotBanned(keyWord);
	}

	public Collection<Request> findRequestNotPassed() {
		return this.requestRepository.findRequestNotPassed();
	}

}
