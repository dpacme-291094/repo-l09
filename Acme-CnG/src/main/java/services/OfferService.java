
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.OfferRepository;
import security.Authority;
import domain.Actor;
import domain.Application;
import domain.Comment;
import domain.Customer;
import domain.Offer;
import forms.FormOffer;

@Service
@Transactional
public class OfferService {

	@Autowired
	private OfferRepository		offerRepository;

	@Autowired
	private CustomerService		customerService;

	@Autowired
	private ApplicationService	applicationService;

	@Autowired
	private Validator			validator;
	@Autowired
	private ActorService		actorService;


	// Constructor --------------------------------------
	public OfferService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Offer create() {

		final Offer o = new Offer();
		o.setBanned(false);
		o.setCustomer(this.customerService.findByPrincipal());
		o.setApplications(new HashSet<Application>());
		return o;
	}

	public Offer save2(final Offer o) {
		Assert.notNull(o);
		return this.offerRepository.save(o);
	}

	// Other Offer methods ---------------------------
	public Offer ban(final Offer o) {
		o.setBanned(true);
		return this.offerRepository.save(o);
	}

	public Collection<Offer> findByPrincipal() {
		// TODO Auto-generated method stub
		Collection<Offer> result = new ArrayList<Offer>();
		final Customer c = this.customerService.findByPrincipal();

		result = this.offerRepository.findByCustomer(c);
		return result;
	}

	public Offer findOne(final int id) {
		// TODO Auto-generated method stub
		return this.offerRepository.findOne(id);
	}

	public Offer edit(final Offer offer) {
		Assert.notNull(offer);
		return this.offerRepository.save(offer);
	}

	public Collection<Offer> findByCustomerAndNotBanned(final Customer id) {
		return this.offerRepository.findByCustomerAndNotBanned(id);
	}

	public Collection<Offer> findAll() {
		return this.offerRepository.findAll();
	}

	public Offer reconstruct(final Offer offer, final BindingResult binding) {
		Offer result;

		if (offer.getId() == 0)
			result = offer;
		else {
			result = this.offerRepository.findOne(offer.getId());

			result.setTitle(offer.getTitle());
			result.setAddressDestination(offer.getAddressDestination());
			result.setAddressOrigin(offer.getAddressOrigin());
			result.setApplications(offer.getApplications());
			result.setBanned(offer.isBanned());
			result.setDescription(offer.getDescription());
			result.setMoment(offer.getMoment());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public Collection<Offer> findAllNotBannedPending() {
		return this.offerRepository.findAllNotBannedPending();
	}

	public Offer reconstruct(final FormOffer formOffer, final BindingResult binding) {
		final Offer result = new Offer();

		result.setTitle(formOffer.getTitle());
		result.setDescription(formOffer.getDescription());
		result.setAddressOrigin(formOffer.getDescription());
		result.setCoordinatesOrigin(formOffer.getCoordinatesOrigin());
		result.setAddressDestination(formOffer.getAddressDestination());
		result.setCoordinatesDestination(formOffer.getCoordinatesDestination());
		result.setMoment(formOffer.getMoment());
		result.setComments(new HashSet<Comment>());
		result.setApplications(new HashSet<Application>());
		result.setCustomer(this.customerService.findByPrincipal());
		result.setPlaces(formOffer.getPlaces());

		this.validator.validate(result, binding);
		return result;
	}
	public void accept(final Offer o) {
		this.checkPrincipal();
		final Customer c = this.customerService.findByPrincipal();
		final Application a = this.applicationService.create(o);
		this.applicationService.save(a);
		final Collection<Application> ca = c.getApplications();
		ca.add(a);
		c.setApplications(ca);
		this.customerService.edit(c);

	}
	public void checkPrincipal() {
		final Actor a = this.actorService.findByPrincipal();
		for (final Authority b : a.getUserAccount().getAuthorities())
			Assert.isTrue(b.getAuthority().equals("CUSTOMER"));

	}

	public Collection<Offer> findByCustomer(final Customer id) {
		return this.offerRepository.findByCustomer(id);
	}

	public void flush() {
		this.offerRepository.flush();
	}

	public Collection<Offer> findOfferByTitleDescritionOrPlacesNotBanned(final String keyWord) {
		return this.offerRepository.findOfferByTitleDescritionOrPlacesNotBanned(keyWord);
	}

	public Collection<Offer> findOfferNotPassed() {
		return this.offerRepository.findOfferNotPassed();
	}

}
