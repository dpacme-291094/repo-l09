
package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AdministratorRepository;
import security.LoginService;
import security.UserAccount;
import domain.Administrator;

@Service
@Transactional
public class AdministratorService {

	@Autowired
	private AdministratorRepository	administratorRepository;
	@Autowired
	private LoginService			loginService;


	public AdministratorService() {
		super();
	}

	public Administrator edit(final Administrator u) {
		Assert.notNull(u);

		final Administrator saved = this.administratorRepository.save(u);

		return saved;
	}

	@SuppressWarnings("static-access")
	public Administrator findByPrincipal() {

		UserAccount userAccount;
		Administrator result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public Administrator findByUserAccount(final UserAccount userAccount) {
		Administrator result;
		result = this.administratorRepository.findByUserAccount(userAccount.getId());
		return result;
	}

	public void flush() {
		this.administratorRepository.flush();
	}

	public Administrator findAdministrator() {
		return this.administratorRepository.findAdministrator();
	}

}
