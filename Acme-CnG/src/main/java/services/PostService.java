
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.PostRepository;
import domain.Post;

@Transactional
@Service
public class PostService {

	@Autowired
	private PostRepository	postRepository;


	public Collection<Object[]> ratioOfferRequest() {
		return this.postRepository.ratioOfferRequest();
	}

	public Double offerPerCustomer() {
		return (this.postRepository.offerPerCustomer() / this.postRepository.numerOfCustomer()) * 1.00;
	}
	public Double requestPerCustomer() {
		return (this.postRepository.requestPerCustomer() / this.postRepository.numerOfCustomer()) * 1.00;
	}
	public Collection<Object[]> applicationsPerPost() {
		return this.postRepository.applicationsPerPost();
	}

	public Post findOne(final Integer id) {
		return this.postRepository.findOne(id);
	}

	public void flush() {
		this.postRepository.flush();
	}

	public void save(final Post b) {
		Assert.notNull(b);
		this.postRepository.save(b);

	}
}
