
package services;

import java.util.Collection;
import java.util.HashSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CustomerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Application;
import domain.Comment;
import domain.Customer;
import domain.Message;
import domain.Post;
import forms.FormCustomer;

@Service
@Transactional
public class CustomerService {

	@Autowired
	private CustomerRepository	customerRepository;

	@Autowired
	private LoginService		loginService;
	@Autowired
	private Validator			validator;


	// Constructor --------------------------------------
	public CustomerService() {
		super();

	}
	public Customer create() {
		final Customer c = new Customer();
		final Authority a = new Authority();
		a.setAuthority("CUSTOMER");
		final UserAccount ua = new UserAccount();
		ua.addAuthority(a);
		c.setUserAccount(ua);
		c.setPosts(new HashSet<Post>());
		c.setApplications(new HashSet<Application>());
		c.setComments(new HashSet<Comment>());
		c.setMessagesIncoming(new HashSet<Message>());
		c.setMessagesOutgoing(new HashSet<Message>());
		return c;
	}

	public Customer save(final Customer c) {
		Assert.notNull(c);
		Customer customer;
		String password;
		String hash;

		password = c.getUserAccount().getPassword();
		hash = this.encodePassword(password);
		c.getUserAccount().setPassword(hash);

		customer = this.customerRepository.save(c);
		return customer;
	}
	public Customer edit(final Customer l) {
		Assert.notNull(l);

		return this.customerRepository.save(l);
	}

	private String encodePassword(final String password) {
		Md5PasswordEncoder encoder;
		String result;

		if (password == null || "".equals(password))
			result = null;
		else {
			encoder = new Md5PasswordEncoder();
			result = encoder.encodePassword(password, null);
		}

		return result;
	}

	// Simple CRUD methods ------------------------------

	public Actor findOne(final Integer id) {
		return this.customerRepository.findOne(id);
	}

	public Collection<Customer> findAll() {
		return this.customerRepository.findAll();
	}

	// Other business methods ---------------------------

	@SuppressWarnings("static-access")
	public Customer findByPrincipal() {
		UserAccount userAccount;
		Customer result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public Customer findByUserAccount(final UserAccount userAccount) {
		Customer result;

		result = this.customerRepository.findByUserAccount(userAccount.getId());
		return result;

	}

	public Collection<Object[]> customerMoreApplicationAccepted() {
		return this.customerRepository.customerMoreApplicationAccepted();
	}

	public Collection<Object[]> customerMoreApplicationDenied() {
		return this.customerRepository.customerMoreApplicationDenied();
	}

	public Customer reconstruct(final FormCustomer formCustomer, final BindingResult binding) {
		final Customer result = new Customer();

		result.setId(formCustomer.getId());
		result.setVersion(formCustomer.getVersion());
		result.setPosts(formCustomer.getPosts());
		result.setApplications(formCustomer.getApplications());
		result.setMessagesIncoming(formCustomer.getMessagesIncoming());
		result.setMessagesOutgoing(formCustomer.getMessagesOutgoing());
		result.setComments(formCustomer.getComments());
		result.setUserAccount(formCustomer.getUserAccount());
		result.setName(formCustomer.getName());
		result.setSurname(formCustomer.getName());
		result.setPhone(formCustomer.getPhone());

		result.setEmail(formCustomer.getEmail());

		this.validator.validate(result, binding);
		return result;
	}

	public Customer reconstruct(final Customer customer, final BindingResult binding) {
		Customer result;

		if (customer.getId() == 0)
			result = customer;
		else {
			result = this.customerRepository.findOne(customer.getId());
			result.setUserAccount(customer.getUserAccount());
			result.setName(customer.getName());
			result.setSurname(customer.getName());
			result.setPhone(customer.getPhone());
			result.setEmail(customer.getEmail());

			this.validator.validate(result, binding);
		}

		return result;
	}
	public FormCustomer createForm() {

		final FormCustomer formTenant = new FormCustomer();

		final Authority a = new Authority();
		final UserAccount userAccount = new UserAccount();

		a.setAuthority("CUSTOMER");
		userAccount.addAuthority(a);

		formTenant.setUserAccount(userAccount);

		return formTenant;
	}

	public void flush() {
		this.customerRepository.flush();
	}
}
