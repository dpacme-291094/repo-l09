
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CommentRepository;
import domain.Comment;

@Transactional
@Service
public class CommentService {

	// Managed repository -------------------------------
	@Autowired
	private CommentRepository	commentRepository;
	@Autowired
	private Validator			validator;


	// Suporting services --------------------------------

	// Constructor --------------------------------------
	public CommentService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Comment create(final Integer rec, final char type) {

		final Comment result = new Comment();
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 60000);
		result.setTitle("");
		result.setText("");
		result.setMoment(currentDate);
		result.setRecipient(rec + "," + type);
		return result;

	}

	public Comment save(final Comment c) {
		Assert.notNull(c);

		final Comment savedComment = this.commentRepository.save(c);

		return savedComment;
	}

	public Comment findOne(final Integer id) {
		return this.commentRepository.findOne(id);
	}

	public Comment reconstruct(final Comment comment, final BindingResult binding) {
		Comment result;

		if (comment.getId() == 0)
			result = comment;
		else {
			result = this.commentRepository.findOne(comment.getId());

			result.setTitle(comment.getTitle());
			result.setMoment(comment.getMoment());
			result.setText(comment.getText());
			result.setStars(comment.getStars());
			result.setRecipient(comment.getRecipient());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public Collection<Comment> findAll() {

		return this.commentRepository.findAll();
	}

	public Collection<Double> calculateAvgCommentFromRequest() {
		return this.commentRepository.calculateAvgCommentFromRequest();
	}

	public Collection<Double> calculateAvgCommentFromOffer() {
		return this.commentRepository.calculateAvgCommentFromOffer();
	}

	public Collection<Double> calculateAvgCommentFromActor() {
		return this.commentRepository.calculateAvgCommentFromActor();
	}

	public Collection<Double> calculateAvgCommentFromCustomer() {
		return this.commentRepository.calculateAvgCommentFromCustomer();
	}

	public Collection<Double> calculateAvgCommentFromAdministrator() {
		return this.commentRepository.calculateAvgCommentFromAdministrator();
	}

	public void flush() {
		this.commentRepository.flush();
	}
}
