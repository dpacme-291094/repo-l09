
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Application;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Integer> {

	@Query("select a from Application a where a.customer.id = ?1")
	Collection<Application> findByCustomer(int id);

	@Query("select a from Application a inner join a.post p inner join p.customer c  where c.id = ?1")
	Collection<Application> findApplicationByCustomer(int id);
}
