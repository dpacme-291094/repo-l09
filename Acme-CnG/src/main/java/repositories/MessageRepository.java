
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

	@Query("select m from Message m where m.actorSender=?1 or m.actorRecipient=?1 order by m.moment desc")
	Collection<Message> findByActor(Actor actor);

	@Query("select m from Message m where m.actorSender.id=?1 order by m.moment desc")
	Collection<Message> findByActorSender(int id);

	@Query("select m from Message m where m.actorRecipient.id=?1 order by m.moment desc")
	Collection<Message> findByActorRecipient(int id);

	@Query("select min(a.messagesOutgoing.size),avg(a.messagesOutgoing.size),max(a.messagesOutgoing.size) from Actor a")
	Collection<Object[]> minAvgMaxMessageSendPerActor();

	@Query("select min(a.messagesIncoming.size),avg(a.messagesIncoming.size),max(a.messagesIncoming.size) from Actor a")
	Collection<Object[]> minAvgMaxMessageReceivedPerActor();

	@Query("select a,a.messagesOutgoing.size from Actor a order by a.messagesOutgoing.size desc")
	Collection<Object[]> actorsSendMaxMessages();

	@Query("select a,a.messagesIncoming.size from Actor a order by a.messagesIncoming.size desc")
	Collection<Object[]> actorsMaxMessages();
}
