
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	@Query("select c from Customer c where c.userAccount.id = ?1")
	Customer findByUserAccount(int id);

	@Query("select a.customer, count(a.status) from Application a where a.status='ACCEPTED' group by a.customer order by count(a.status) desc")
	Collection<Object[]> customerMoreApplicationAccepted();

	@Query("select a.customer, count(a.status) from Application a where a.status='DENIED' group by a.customer order by count(a.status) desc")
	Collection<Object[]> customerMoreApplicationDenied();

}
