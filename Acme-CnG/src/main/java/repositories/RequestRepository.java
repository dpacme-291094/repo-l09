
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Customer;
import domain.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {

	@Query("select r.customer,sum(1.0) from Request r group by r.customer")
	public Collection<Object[]> calculateAvgRequest();

	@Query("select r from Request r where r.customer=?1 and r.banned=false")
	Collection<Request> findByCustomerAndNotBanned(Customer c);

	@Query("select r from Request r where r.banned=false and r.places>0")
	Collection<Request> findAllNotBannedPending();

	@Query("select r from Request r where r.customer=?1")
	Collection<Request> findByCustomer(Customer c);

	@Query("select r from Request r WHERE  r.banned=false AND (r.title LIKE concat('%',?1,'%') OR r.description LIKE concat('%',?1,'%') OR r.addressOrigin LIKE concat('%',?1,'%') OR r.addressDestination LIKE concat('%',?1,'%'))")
	Collection<Request> findRequestByTitleDescritionOrPlacesNotBanned(String keyWord);

	@Query("select o from Request o WHERE  o.moment > CURRENT_DATE")
	Collection<Request> findRequestNotPassed();
}
