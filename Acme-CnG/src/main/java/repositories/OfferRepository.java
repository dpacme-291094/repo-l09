
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Customer;
import domain.Offer;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Integer> {

	@Query("select o from Offer o where o.customer=?1")
	Collection<Offer> findByCustomer(Customer c);

	@Query("select o from Offer o where o.customer=?1 and o.banned=false")
	Collection<Offer> findByCustomerAndNotBanned(Customer c);

	@Query("select o from Offer o where o.banned=false and o.places>0")
	Collection<Offer> findAllNotBannedPending();

	@Query("select o from Offer o WHERE  o.banned=false AND (o.title LIKE concat('%',?1,'%') OR o.description LIKE concat('%',?1,'%') OR o.addressOrigin LIKE concat('%',?1,'%') OR o.addressDestination LIKE concat('%',?1,'%'))")
	Collection<Offer> findOfferByTitleDescritionOrPlacesNotBanned(String keyWord);

	@Query("select o from Offer o WHERE  o.moment > CURRENT_DATE")
	Collection<Offer> findOfferNotPassed();

}
