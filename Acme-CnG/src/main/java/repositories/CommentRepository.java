
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	@Query("select avg(r.comments.size) from Request r")
	public Collection<Double> calculateAvgCommentFromRequest();

	@Query("select avg(o.comments.size) from Offer o")
	public Collection<Double> calculateAvgCommentFromOffer();

	@Query("select avg(a.comments.size) from Actor a")
	public Collection<Double> calculateAvgCommentFromActor();

	@Query("select avg(c.comments.size) from Customer c")
	public Collection<Double> calculateAvgCommentFromCustomer();

	@Query("	select avg(a.comments.size) from Administrator a")
	public Collection<Double> calculateAvgCommentFromAdministrator();

}
