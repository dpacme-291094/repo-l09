
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

	@Query("select sum(case when p in (select o from Offer o) then 1.0 else 0.0 end) as Offer, sum(case when p in (select r from Request r) then 1.0 else 0.0 end) as Request, sum(case when p in (select o from Offer o) then 1.0 else 0.0 end)/sum(case when p in (select r from Request r) then 1.0 else 0.0 end) as Ratio from Post p")
	public Collection<Object[]> ratioOfferRequest();

	@Query("select count(o) from Offer o")
	public Double offerPerCustomer();

	@Query("select count(r) from Request r")
	public Double requestPerCustomer();

	@Query("select count(r) from Customer r")
	public Double numerOfCustomer();

	@Query("select avg(p.applications.size) from Post p")
	public Collection<Object[]> applicationsPerPost();

}
