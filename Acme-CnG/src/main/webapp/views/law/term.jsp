<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div>


<h2><spring:message code="law.term.title1"/></h2>
<p><spring:message code="law.term.context1"/></p>

<h2><spring:message code="law.term.title2"/></h2>
<p><spring:message code="law.term.context2"/></p>

<h2><spring:message code="law.term.title3"/></h2>
<p><spring:message code="law.term.context3"/></p>

<h2><spring:message code="law.term.title4"/></h2>
<p><spring:message code="law.term.context4"/></p>

<h2><spring:message code="law.term.title5"/></h2>
<p><spring:message code="law.term.context5"/></p>

<h2><spring:message code="law.term.title6"/></h2>
<p><spring:message code="law.term.context6"/></p>

<h2><spring:message code="law.term.title7"/></h2>
<p><spring:message code="law.term.context7"/></p>








</div>