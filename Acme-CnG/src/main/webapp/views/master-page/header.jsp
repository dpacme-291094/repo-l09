<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript">
	function relativeRedir(loc) {
		var b = document.getElementsByTagName('base');
		if (b && b[0] && b[0].href) {
			if (b[0].href.substr(b[0].href.length - 1) == '/' && loc.charAt(0) == '/')
				loc = loc.substr(1);
			loc = b[0].href + loc;
		}
		window.location.replace(loc);
	}
</script>


<div>

	<a href="http://localhost:8080/Acme-CnG/"><img
		src="images/logo.png" alt="Acme CnG co., Inc." /></a>
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->

		<security:authorize access="hasRole('ADMINISTRATOR')">
			<li><a class="fNiv"><spring:message
						code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>

					<li><a href="configuration/administrator/edit.do"><spring:message
								code="master.page.configuration" /></a></li>
					<li><a href="comment/administrator/list.do"><spring:message
								code="master.page.ban.comment" /></a></li>
					<li><a href="offer/administrator/listBan.do"><spring:message
								code="master.page.ban.offer" /></a></li>
					<li><a href="request/administrator/listBan.do"><spring:message
								code="master.page.ban.request" /></a></li>
				</ul></li>

			<li><a class="fNiv" href="offer/administrator/list.do"><spring:message
						code="master.page.offer" /></a></li>
			<li><a class="fNiv" href="request/administrator/list.do"><spring:message
						code="master.page.request" /></a></li>

			<li><a href="report/administrator/dashboard.do"><spring:message
						code="master.page.dashboard" /></a></li>


		</security:authorize>



		<security:authorize access="hasRole('CUSTOMER')">
			<li><a class="fNiv"><spring:message
						code="master.page.applications" /></a>
				<ul>
					<li><a href="application/customer/privateList.do"><spring:message
								code="master.page.myapplication" /></a></li>
					<li><a href="application/customer/list.do"><spring:message
								code="master.page.pendingapplication" /></a></li>
				</ul></li>
		</security:authorize>

		<security:authorize access="hasRole('CUSTOMER')">
			<li><a class="fNiv"><spring:message code="master.page.offer" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="offer/customer/list.do"><spring:message
								code="master.page.customer.offer" /></a></li>

					<li><a href="offer/customer/search.do?keyWord="><spring:message
								code="master.page.offer.search" /></a></li>
				</ul></li>
		</security:authorize>

		<security:authorize access="hasRole('CUSTOMER')">
			<li><a class="fNiv"><spring:message
						code="master.page.request" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="request/customer/list.do"><spring:message
								code="master.page.customer.request" /></a></li>

					<li><a href="request/customer/search.do?keyWord="><spring:message
								code="master.page.request.search" /></a></li>
				</ul></li>
		</security:authorize>

		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message
						code="master.page.login" /></a></li>
			<li><a class="fNiv"> <spring:message
						code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="customer/register.do"><spring:message
								code="master.page.customer" /></a></li>

				</ul></li>
		</security:authorize>

		<security:authorize access="isAuthenticated()">
			<li><a class="fNiv"> <spring:message
						code="master.page.message" />
			</a>
				<ul>
					<li class="arrow"></li>

					<li><a href="message/actor/inbox.do"><spring:message
								code="master.page.inbox" /> </a></li>
					<li><a href="message/actor/outbox.do"><spring:message
								code="master.page.outbox" /> </a></li>
				</ul></li>
			<li><a class="fNiv"> <spring:message
						code="master.page.profile" /> (<security:authentication
						property="principal.username" />)
			</a>
				<ul>
					<li class="arrow"></li>
					<security:authorize access="hasRole('CUSTOMER')">
						<li><a href="customer/profileForOwner.do"><spring:message
									code="master.page.profile" /></a></li>
					</security:authorize>

					<li><a href="administrator/profileAdmin.do"><spring:message
								code="master.page.profile.admin" /></a></li>
					<li><a href="j_spring_security_logout"><spring:message
								code="master.page.logout" /> </a></li>
				</ul></li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>


<!--//BLOQUE COOKIES-->
<div id="barraaceptacion" style="display: block;">
	<div class="inner">
		<spring:message code="master.page.law.solicitud" />
		<a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a>
		| <a href="law/cookies.do" target="_blank" class="info"><spring:message
				code="master.page.information" /></a> <a href="law/term.do"
			target="_blank" class="info"><spring:message
				code="master.page.law.term" /></a>
	</div>
</div>

<div id="cookies">
	<script>
		function getCookie(c_name) {
			var c_value = document.cookie;
			var c_start = c_value.indexOf(" " + c_name + "=");
			if (c_start == -1) {
				c_start = c_value.indexOf(c_name + "=");
			}
			if (c_start == -1) {
				c_value = null;
			} else {
				c_start = c_value.indexOf("=", c_start) + 1;
				var c_end = c_value.indexOf(";", c_start);
				if (c_end == -1) {
					c_end = c_value.length;
				}
				c_value = unescape(c_value.substring(c_start, c_end));
			}
			return c_value;
		}

		function setCookie(c_name, value, exdays) {
			var exdate = new Date();
			exdate.setDate(exdate.getDate() + exdays);
			var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
			document.cookie = c_name + "=" + c_value;
		}

		if (getCookie('tiendaaviso') != "1") {
			document.getElementById("barraaceptacion").style.display = "block";
		} else {
			document.getElementById("barraaceptacion").style.display = "none";
		}
		function PonerCookie() {
			setCookie('tiendaaviso', '1', 365);
			document.getElementById("barraaceptacion").style.display = "none";
		}
	</script>
</div>
