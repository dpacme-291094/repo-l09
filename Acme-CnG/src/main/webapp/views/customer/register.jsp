<%--
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="customer/register.do" modelAttribute="formCustomer">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount.Authorities" />
	<form:hidden path="messagesIncoming"/>
	<form:hidden path="messagesOutgoing"/>
	<form:hidden path="posts" />
	<form:hidden path="applications" />
	<table id="formus">
		
		<acme:textbox code="customer.name" path="name" />
		<acme:textbox code="customer.surname" path="surname" />
		<acme:textbox code="customer.email" path="email" />
		<acme:textbox code="customer.phone" path="phone" />
	
		<acme:textbox code="customer.userAccount" path="userAccount.username" />

		<acme:password code="customer.password" path="userAccount.password" />
		<tr>
			<td><b><spring:message code="customer.repassword" /></b></td>
			<td><input type="password" name="repassword" /></td>
		</tr>
	</table>

	<input type="checkbox" name="terms">

	<spring:message code="customer.terms" />
	<br />
	<br />

	<acme:submit name="register" code="customer.register" />
	<acme:cancel url="welcome/index.do" code="customer.cancel" />
</form:form>



