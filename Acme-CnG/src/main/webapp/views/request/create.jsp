<%--
 * edit.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="request/customer/create.do"
	modelAttribute="FormRequest">

	<table id="formus">
		<acme:textbox code="request.title" path="title" />
		<acme:textbox code="request.description" path="description" />
		<acme:selectModified code="request.places" path="places"
			items="${places}" id="places" />

		<acme:textbox code="request.addressOrigin" path="addressOrigin" />
		<acme:textbox code="request.coordinatesOriginla"
			path="coordinatesOrigin.latitude" />
		<acme:textbox code="request.coordinatesOriginlo"
			path="coordinatesOrigin.longitude" />
		<acme:textbox code="request.addressDestination"
			path="addressDestination" />
		<acme:textbox code="request.coordinatesDestinationla"
			path="coordinatesDestination.latitude" />
		<acme:textbox code="request.coordinatesDestinationlo"
			path="coordinatesDestination.longitude" />
		<acme:textbox code="request.moment" path="moment" />
	</table>

	<acme:submit code="request.new" name="save" />
	<acme:cancel code="request.cancel" url="request/customer/list.do" />

</form:form>
