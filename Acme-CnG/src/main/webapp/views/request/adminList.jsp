<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<display:table name="request" id="row"
	requestURI="request/administrator/list.do" pagesize="5"
	class="displaytag">

	<display:column property="title" titleKey="request.title"
		sortable="true" />
	<display:column property="description" titleKey="request.description"
		sortable="false" />
	<display:column property="places" titleKey="request.places"
		sortable="false" />
	<display:column property="addressOrigin"
		titleKey="request.addressOrigin" sortable="false" />
	<display:column property="addressDestination"
		titleKey="request.addressDestination" sortable="false" />
	<display:column property="moment" titleKey="request.moment"
		sortable="true" />
	<display:column property="customer.name" titleKey="request.customer" />
	<security:authorize access="hasRole('ADMINISTRATOR')">
		
		<display:column titleKey="request.ban" sortable="true">
			<jstl:if test="${row.banned==false}">
				<jstl:out value="No" />
			</jstl:if>
			<jstl:if test="${row.banned==true}">
				<spring:message code="request.comment.true" />
			</jstl:if>
		</display:column>
		
	</security:authorize>

	<display:column>

		<security:authorize access="hasRole('CUSTOMER')">
			<a href="request/customer/view.do?requestId=${row.id}"><spring:message
					code="request.view" /></a>
		</security:authorize>
		<security:authorize access="isAnonymous() || hasRole('ADMINISTRATOR')">
			<a href="request/view.do?requestId=${row.id}"><spring:message
					code="request.view" /></a>
		</security:authorize>

	</display:column>

</display:table>
