<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<table>
	<tr>
		<td><b><spring:message code="request.title" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${request.title}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="request.description" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${request.description}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="request.addressOrigin" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${request.addressOrigin}" /></td>
	</tr>
	<jstl:if test="${request.coordinatesOrigin.latitude != null }">
		<tr>
			<td><b><spring:message code="request.coordinatesOriginla" /></b></td>
		</tr>
		<tr>
			<td><jstl:out value="${request.coordinatesOrigin.latitude}" /></td>
		</tr>
		<tr>
			<td><b><spring:message code="request.coordinatesOriginlo" /></b></td>
		</tr>
		<tr>
			<td><jstl:out value="${request.coordinatesOrigin.longitude}" /></td>
		</tr>
	</jstl:if>
	<tr>
		<td><b><spring:message code="request.addressDestination" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${request.addressDestination}" /></td>
	</tr>
	<jstl:if test="${request.coordinatesDestination.latitude != null }">
		<tr>
			<td><b><spring:message
						code="request.coordinatesDestinationla" /></b></td>
		</tr>
		<tr>
			<td><jstl:out value="${request.coordinatesDestination.latitude}" /></td>
		</tr>
		<tr>
			<td><b><spring:message
						code="request.coordinatesDestinationlo" /></b></td>
		</tr>
		<tr>
			<td><jstl:out
					value="${request.coordinatesDestination.longitude}" /></td>
		</tr>
	</jstl:if>
	<tr>
		<td><b><spring:message code="request.moment" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${request.moment}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="request.customer" /></b></td>
	</tr>
	<tr>
		<td><a
			href="customer/profile.do?customerId=${request.customer.id}"><jstl:out
					value="${request.customer.name}" /></a></td>
	</tr>

</table>

<security:authorize access="hasRole('CUSTOMER')">
	<jstl:if
		test="${!(isnotcustomer==request.customer.id) && request.places>0}">
		<form:form method="POST" action="request/customer/view.do">
			<input type="hidden" name="request" value="${request.id}" />
			<input type="submit" name="accept"
				value="<spring:message code="request.accept" />" />&nbsp; 
	</form:form>
	</jstl:if>
</security:authorize>

<table id="formus">
	<tr>
		<td><u>
				<h3>
					<spring:message code="request.comment.show" />

				</h3>
		</u></td>
	</tr>

	<jstl:forEach items="${request.comments}" var="comment">
		<jstl:if test="${comment.banned == false || showBanComment == true}">


			<tr>
				<td><b><spring:message code="comment.title" /> </b>:&nbsp; <jstl:out
						value="${comment.title}" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="comment.stars" /></b> :&nbsp; <jstl:out
						value="${comment.stars}" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="comment.moment" /></b> :&nbsp; <jstl:out
						value="${comment.moment}" /></td>
			</tr>

			<tr>
				<td><b><spring:message code="comment.text" /> </b>:&nbsp; <jstl:out
						value="${comment.text}" /></td>
			</tr>
			<tr>
				<td><br /></td>
			</tr>
		</jstl:if>
	</jstl:forEach>

</table>
<security:authorize access="isAuthenticated()">
	<div>
		<a href="comment/actor/create.do?rec=${request.id}&type=P"><spring:message
				code="offer.commment.write" /></a>
	</div>
</security:authorize>

