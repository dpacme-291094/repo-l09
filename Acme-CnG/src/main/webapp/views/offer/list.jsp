<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<display:table name="offer" id="row" requestURI="offer/customer/list.do"
	pagesize="5" class="displaytag">

	<display:column property="title" titleKey="offer.title" sortable="true" />
	<display:column property="description" titleKey="offer.description"
		sortable="false" />
	<display:column property="places" titleKey="offer.places"
		sortable="false" />
	<display:column property="addressOrigin" titleKey="offer.addressOrigin"
		sortable="false" />
	<display:column property="addressDestination"
		titleKey="offer.addressDestination" sortable="false" />
	<display:column property="moment" titleKey="offer.moment"
		sortable="true" />
		
	<display:column titleKey="offer.ban" sortable="true">
			<jstl:if test="${row.banned==false}">
				<jstl:out value="No" />
			</jstl:if>
			<jstl:if test="${row.banned==true}">
				<spring:message code="offer.comment.true" />
			</jstl:if>
		</display:column>
	
	<display:column>
		<a href="offer/view.do?offerId=${row.id}"><spring:message
				code="offer.view" /></a>
	</display:column>
</display:table>

<a href="offer/customer/create.do"><spring:message
		code="offer.create" /></a>