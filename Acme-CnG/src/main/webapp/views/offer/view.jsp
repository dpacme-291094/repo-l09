<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<table>
	<tr>
		<td><b><spring:message code="offer.title" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${offer.title}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="offer.description" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${offer.description}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="offer.addressOrigin" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${offer.addressOrigin}" /></td>
	</tr>
	<jstl:if test="${offer.coordinatesOrigin.latitude != null }">
		<tr>
			<td><b><spring:message code="offer.coordinatesOriginla" /></b></td>
		</tr>
		<tr>
			<td><jstl:out value="${offer.coordinatesOrigin.latitude}" /></td>
		</tr>
		<tr>
			<td><b><spring:message code="offer.coordinatesOriginlo" /></b></td>
		</tr>
		<tr>
			<td><jstl:out value="${offer.coordinatesOrigin.longitude}" /></td>
		</tr>
	</jstl:if>
	<tr>
		<td><b><spring:message code="offer.addressDestination" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${offer.addressDestination}" /></td>
	</tr>
	<jstl:if test="${offer.coordinatesDestination.latitude != null }">
		<tr>
			<td><b><spring:message code="offer.coordinatesDestinationla" /></b></td>
		</tr>
		<tr>
			<td><jstl:out value="${offer.coordinatesDestination.latitude}" /></td>
		</tr>
		<tr>
			<td><b><spring:message code="offer.coordinatesDestinationlo" /></b></td>
		</tr>
		<tr>
			<td><jstl:out value="${offer.coordinatesDestination.longitude}" /></td>
		</tr>
	</jstl:if>
	<tr>
		<td><b><spring:message code="offer.moment" /></b></td>
	</tr>
	<tr>
		<td><jstl:out value="${offer.moment}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="offer.customer" /></b></td>
	</tr>
	<tr>
		<td><a href="customer/profile.do?customerId=${offer.customer.id}"><jstl:out
					value="${offer.customer.name}" /></a></td>
	</tr>

</table>

<security:authorize access="hasRole('CUSTOMER')">
	<jstl:if
		test="${!(isnotcustomer==offer.customer.id) && offer.places>0}">
		<form:form method="POST" action="offer/customer/view.do">
			<input type="hidden" name="offer" value="${offer.id}" />
			<input type="submit" name="accept"
				value="<spring:message code="offer.accept" />" />&nbsp; 
	</form:form>
	</jstl:if>
</security:authorize>
<br />
<table id="formus">
	<tr>
		<td><u>
				<h3>
					<spring:message code="offer.comment.show" />

				</h3>
		</u></td>
	</tr>
	<jstl:forEach items="${offer.comments}" var="comment">
		<jstl:if test="${comment.banned == false || showBanComment == true}">


			<tr>
				<td><b><spring:message code="comment.title" /> </b>:&nbsp; <jstl:out
						value="${comment.title}" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="comment.stars" /></b> :&nbsp; <jstl:out
						value="${comment.stars}" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="comment.moment" /></b> :&nbsp; <jstl:out
						value="${comment.moment}" /></td>
			</tr>

			<tr>
				<td><b><spring:message code="comment.text" /> </b>:&nbsp; <jstl:out
						value="${comment.text}" /></td>
			</tr>
			<tr>
				<td><br /></td>
			</tr>
		</jstl:if>
	</jstl:forEach>
</table>

<security:authorize access="isAuthenticated()">
	<div>
		<a href="comment/actor/create.do?rec=${offer.id}&type=P"><spring:message
				code="offer.commment.write" /></a>
	</div>
</security:authorize>