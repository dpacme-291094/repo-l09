<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<form action="offer/customer/search.do?keyword=">
	<spring:message code="offer.search.requirement" />
	<br>
	<spring:message code="offer.insert.keyword" />
	<input type="text" name="keyWord"><br> <input
		type="submit" value="<spring:message code="offer.search" />">

</form>

<display:table name="offers" id="row"
	requestURI="offer/customer/search.do" class="displaytag" pagesize="5">

	<display:column property="title" titleKey="offer.title" sortable="true" />
	<display:column property="description" titleKey="offer.description"
		sortable="false" />
	<display:column property="places" titleKey="offer.places"
		sortable="false" />
	<display:column property="addressOrigin" titleKey="offer.addressOrigin"
		sortable="false" />
	<display:column property="addressDestination"
		titleKey="offer.addressDestination" sortable="false" />
	<display:column property="moment" titleKey="offer.moment"
		sortable="true" />
	<display:column property="customer.name" titleKey="offer.customer" />

	<display:column>
		<a href="offer/customer/view.do?offerId=${row.id}"><spring:message
				code="offer.view" /></a>
	</display:column>

</display:table>

