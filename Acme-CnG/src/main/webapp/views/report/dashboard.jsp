<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- C Queries -->

<h2>
	<spring:message code="report.ratioOfferRequest" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.offer" />:</th>
		<th><spring:message code="report.request" />:</th>
		<th><spring:message code="report.ratio" />:</th>
	</tr>

	<jstl:forEach items="${ror}" var="rori">
		<tr>


			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${rori[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${rori[1]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${rori[2]}" /></td>
		</tr>
	</jstl:forEach>

</table>

<h2>
	<spring:message code="report.offerAndRequestPerCustomer" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.offer" />:</th>
		<th><spring:message code="report.request" />:</th>
	</tr>
	<tr>
		<td><fmt:formatNumber type="number" maxFractionDigits="2"
				value="${opc}" /></td>
		<td><fmt:formatNumber type="number" maxFractionDigits="2"
				value="${rpc}" /></td>
	</tr>

</table>


<h2>
	<spring:message code="report.applicationsPerPost" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.avg" />:</th>
	</tr>
	<jstl:forEach items="${app}" var="appi">
		<tr>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${appi}" /></td>
		</tr>
	</jstl:forEach>
</table>

<h2>
	<spring:message code="report.customerMoreApplicationAccepted" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.customer" />:</th>
		<th><spring:message code="report.application" />:</th>
	</tr>
	<jstl:forEach items="${cmaa}" var="cmaai">
		<tr>
			<td><jstl:out value="${cmaai[0].name}" /> <jstl:out
					value="${cmaai[0].surname}" /></td>
			<td><jstl:out value="${cmaai[1]}" /></td>
		</tr>
	</jstl:forEach>
</table>

<h2>
	<spring:message code="report.customerMoreApplicationDenied" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.customer" />:</th>
		<th><spring:message code="report.application" />:</th>
	</tr>
	<jstl:forEach items="${cmad}" var="cmadi">
		<tr>
			<td><jstl:out value="${cmadi[0].name}" /> <jstl:out
					value="${cmadi[0].surname}" /></td>
			<td><jstl:out value="${cmadi[1]}" /></td>
		</tr>
	</jstl:forEach>
</table>

<h2>
	<spring:message code="report.averageComments" />
	:
</h2>

<table>
	<tr>
		<td>
			<table>
				<tr>
					<th><spring:message code="report.request" />:</th>

				</tr>
				<jstl:forEach items="${cacr}" var="cacri">
					<tr>
						<td><fmt:formatNumber type="number" maxFractionDigits="2"
								value="${cacri}" /></td>
					</tr>
				</jstl:forEach>
			</table>
		</td>

		<td>
			<table>
				<tr>
					<th><spring:message code="report.offer" />:</th>

				</tr>
				<jstl:forEach items="${caco}" var="cacoi">
					<tr>
						<td><fmt:formatNumber type="number" maxFractionDigits="2"
								value="${cacoi}" /></td>
					</tr>
				</jstl:forEach>
			</table>
		</td>

		<td>
			<table>
				<tr>
					<th><spring:message code="report.actor" />:</th>

				</tr>
				<jstl:forEach items="${caca}" var="cacai">
					<tr>
						<td><fmt:formatNumber type="number" maxFractionDigits="2"
								value="${cacai}" /></td>
					</tr>
				</jstl:forEach>
			</table>
		</td>
	</tr>

</table>



<h2>
	<spring:message code="report.averageCommentsActor" />
	:
</h2>

<table>
	<tr>
		<td>
			<table>
				<tr>
					<th><spring:message code="report.customer" />:</th>

				</tr>
				<jstl:forEach items="${cacc}" var="cacci">
					<tr>
						<td><fmt:formatNumber type="number" maxFractionDigits="2"
								value="${cacci}" /></td>
					</tr>
				</jstl:forEach>
			</table>
		</td>

		<td>
			<table>
				<tr>
					<th><spring:message code="report.administrator" />:</th>

				</tr>
				<jstl:forEach items="${cacaa}" var="cacaai">
					<tr>
						<td><fmt:formatNumber type="number" maxFractionDigits="2"
								value="${cacaai}" /></td>
					</tr>
				</jstl:forEach>
			</table>
		</td>
	</tr>
</table>


<h2>
	<spring:message code="report.percentCommentsActor" />
	:
</h2>

<table>
	<tr>
		<td>
			<table>
				<tr>
					<th><spring:message code="report.more" />:</th>
					<th><spring:message code="report.avg" />:</th>
				</tr>
				<jstl:forEach items="${mtp}" var="mtpi">
					<tr>
						<td><jstl:out value="${mtpi.name}" /> <jstl:out
								value="${mtpi.surname}" /></td>
						<td><fmt:formatNumber type="number" maxFractionDigits="2"
								value="${mtpi.comments.size()}" /></td>
					</tr>
				</jstl:forEach>
			</table>
		</td>

		<td>
			<table>
				<tr>
					<th><spring:message code="report.less" />:</th>
					<th><spring:message code="report.avg" />:</th>

				</tr>
				<jstl:forEach items="${ltp}" var="ltpi">
					<tr>
						<td><jstl:out value="${ltpi.name}" /> <jstl:out
								value="${ltpi.surname}" /></td>
						<td><fmt:formatNumber type="number" maxFractionDigits="2"
								value="${ltpi.comments.size()}" /></td>
					</tr>
				</jstl:forEach>
			</table>
		</td>
	</tr>
</table>

<h2>
	<spring:message code="report.minAvgMaxSendActor" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.min" />:</th>
		<th><spring:message code="report.avg" />:</th>
		<th><spring:message code="report.max" />:</th>
	</tr>

	<jstl:forEach items="${consultaA1}" var="consultaA1">
		<tr>

			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${consultaA1[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${consultaA1[1]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${consultaA1[3]}" /></td>
		</tr>
	</jstl:forEach>

</table>

<h2>
	<spring:message code="report.minAvgMaxMessageReceivedPerActor" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.min" />:</th>
		<th><spring:message code="report.avg" />:</th>
		<th><spring:message code="report.max" />:</th>
	</tr>

	<jstl:forEach items="${consultaA2}" var="consultaA2">
		<tr>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${consultaA2[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${consultaA2[1]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${consultaA2[2]}" /></td>
		</tr>
	</jstl:forEach>

</table>

<h2>
	<spring:message code="report.actorsSendMaxMessages" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.actor" />:</th>
		<th><spring:message code="report.max" />:</th>
	</tr>
	<jstl:forEach items="${consultaA3}" var="consultaA3">
		<tr>
			<td><jstl:out value="${consultaA3[0].name}" /> <jstl:out
					value="${consultaA3[0].surname}" /></td>
			<td><jstl:out value="${consultaA3[1]}" /></td>
		</tr>
	</jstl:forEach>
</table>

<h2>
	<spring:message code="report.actorsMaxMessages" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.actor" />:</th>
		<th><spring:message code="report.max" />:</th>
	</tr>
	<jstl:forEach items="${consultaA4}" var="consultaA4">
		<tr>
			<td><jstl:out value="${consultaA4[0].name}" /> <jstl:out
					value="${consultaA4[0].surname}" /></td>
			<td><jstl:out value="${consultaA4[1]}" /></td>
		</tr>
	</jstl:forEach>
</table>