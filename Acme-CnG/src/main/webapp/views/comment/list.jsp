<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="comments" id="row"
	requestURI="comment/administrator/list.do" pagesize="5"
	class="displaytag">
	<display:column property="title" titleKey="comment.title">
	</display:column>

	<display:column property="text" titleKey="comment.text">
	</display:column>

	<display:column property="moment" titleKey="comment.moment"
		sortable="true">
	</display:column>

	<display:column>
		<jstl:if test="${row.banned==false}">
			<form:form method="POST" action="comment/administrator/list.do">
				<input type="hidden" name="comment" value="${row.id}" />

				<input type="submit" name="ban"
					value="<spring:message code="comment.ban" />" />
			</form:form>
		</jstl:if>
		<jstl:if test="${row.banned==true}">
			<spring:message code="banned.true" />

		</jstl:if>
	</display:column>

</display:table>
