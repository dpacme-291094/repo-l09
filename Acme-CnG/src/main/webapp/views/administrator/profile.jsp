<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<table id="formus">

	<tr>
		<td><b><spring:message code="administrator.name" /></b> <br />
			<jstl:out value="${administrator.name}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="administrator.surname" /></b> <br />
			<jstl:out value="${administrator.surname}" /></td>
	</tr>


	<tr>
		<td><b><spring:message code="administrator.email" /></b> <br />
			<jstl:out value="${administrator.email}" /></td>
	</tr>



	<tr>
		<td><b><spring:message code="administrator.phone" /></b> <br />
			<jstl:out value="${administrator.phone}" /></td>
	</tr>

</table>
<table id="formus">

	<tr>
		<td><u>
				<h3>
					<spring:message code="administrator.comments" />

				</h3>
		</u></td>
	</tr>

	<jstl:forEach items="${administrator.comments}" var="comment">
		<jstl:if test='${hiddenComment != true || comment.banned == false}'>
			<table>
				<tr>
					<td><b><spring:message code="comment.title" /> </b>:&nbsp; <jstl:out
							value="${comment.title}" /></td>
				</tr>
				<tr>
					<td><b><spring:message code="comment.stars" /></b> :&nbsp; <jstl:out
							value="${comment.stars}" /></td>
				</tr>
				<tr>
					<td><b><spring:message code="comment.moment" /></b> :&nbsp; <jstl:out
							value="${comment.moment}" /></td>
				</tr>

				<tr>
					<td><b><spring:message code="comment.text" /> </b>:&nbsp; <jstl:out
							value="${comment.text}" /></td>
				</tr>
			</table>
		</jstl:if>
	</jstl:forEach>

	<tr>
		<td><security:authorize access="isAuthenticated()">
				<div>
					<a href="comment/actor/create.do?rec=${administrator.id}&type=A"><spring:message
							code="administrator.comment.write" /></a>
				</div>
			</security:authorize></td>
	</tr>
</table>