<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<jsp:useBean id="today" class="java.util.Date" />


<display:table name="applications" id="row"
	requestURI="application/customer/list.do" pagesize="5"
	class="displaytag">

	<display:column property="post.title" titleKey="post.title"
		sortable="true">

	</display:column>


	<display:column titleKey="application.customer" sortable="false">
		<a href="customer/profile.do?customerId=${row.customer.id}"> <jstl:out
				value="${row.customer.name}" /></a>
	</display:column>
	<display:column property="moment" titleKey="application.moment" />

	<display:column property="status" titleKey="application.status"
		sortable="false" />
	<display:column>
		<jstl:if test="${row.status == 'ACCEPTED'}">
			<spring:message code="application.accepted" />
		</jstl:if>
		<jstl:if test="${row.status == 'DENIED'}">
			<spring:message code="application.denied" />
		</jstl:if>




		<jstl:if test="${row.status == 'PENDING'}">


			<jstl:if test="${! (row.moment lt today) }">


				<form:form method="POST" action="application/customer/list.do">
					<input type="hidden" name="application" value="${row.id}" />
					<input type="submit" name="accepted"
						value="<spring:message code="application.accept" />" />&nbsp; 
					<input type="hidden" name="application" value="${row.id}" />
					<input type="submit" name="denied"
						value="<spring:message code="application.deny" />"
						onclick="return confirm('<spring:message code="application.confirm.denied" />')" />&nbsp;
			</form:form>
			</jstl:if>
			<jstl:if test="${(row.moment lt today) }">
				<spring:message code="application.moment.error" />
			</jstl:if>
		</jstl:if>




	</display:column>
</display:table>
