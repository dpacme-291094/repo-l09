<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ page import="java.io.*,java.util.*,javax.servlet.*"%>
<%
	Date date = new Date();
%>


<display:table name="applications" id="row"
	requestURI="application/customer/privateList.do" pagesize="5"
	class="displaytag">

	<display:column property="post.title" titleKey="post.title"
		sortable="true">

	</display:column>


	<display:column property="customer.name"
		titleKey="application.customer" sortable="false" />
	<display:column property="status" titleKey="application.status"
		sortable="false" />
	<display:column property="moment" titleKey="application.moment" />
	

	

</display:table>
