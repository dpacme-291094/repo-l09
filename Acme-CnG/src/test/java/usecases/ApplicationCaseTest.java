
package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ApplicationService;
import utilities.AbstractTest;
import domain.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class ApplicationCaseTest extends AbstractTest {

	@Autowired
	private ApplicationService	applicationService;


	//Accept an application sent to any of my posts
	/*
	 * En el siguiente orden:
	 * El customer del post acepta una petici�n (Correcto)
	 * Intenta aceptar una petici�n que no existe (Fallo)
	 * El customer que ha enviado la petici�n intenta aceptarla (Fallo)
	 * Alguien sin loguear intenta aceptar una petici�n (Fallo)
	 */
	@Test
	public void driverAccept() {

		final Object testingData[][] = {

			{
				"customer2", 52, null
			}, {
				"customer2", 60, NullPointerException.class
			}, {
				"customer1", 52, IllegalArgumentException.class
			}, {
				null, 52, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateAccept((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	//Deny an application sent to any of my posts
	/*
	 * En el siguiente orden:
	 * El customer del post deniega una petici�n (Correcto)
	 * Intenta denegar una petici�n que no existe (Fallo)
	 * El customer que ha enviado la petici�n intenta denegarla (Fallo)
	 * Alguien sin loguear intenta denegar una petici�n (Fallo)
	 */
	@Test
	public void driverDeny() {

		final Object testingData[][] = {

			{
				"customer2", 52, null
			}, {
				"customer2", 60, NullPointerException.class
			}, {
				"customer1", 52, IllegalArgumentException.class
			}, {
				null, 52, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDeny((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateAccept(final String username, final Integer applicationId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Application a = this.applicationService.findOne(applicationId);
			this.applicationService.accept(a);
			this.applicationService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateDeny(final String username, final Integer applicationId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Application a = this.applicationService.findOne(applicationId);
			this.applicationService.deny(a);
			this.applicationService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
