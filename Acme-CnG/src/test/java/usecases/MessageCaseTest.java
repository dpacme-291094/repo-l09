
package usecases;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.CustomerService;
import services.MessageService;
import utilities.AbstractTest;
import domain.Customer;
import domain.Message;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class MessageCaseTest extends AbstractTest {

	@Autowired
	private MessageService	messageService;
	@Autowired
	private CustomerService	customerService;


	//Exchange messages with another actors
	/*
	 * En el siguiente orden comprueba:
	 * Envio de un mensaje con todos los datos correctos.
	 * Envio de un mensaje con un customer erroneo.
	 * Envio de un mensaje a un email que no existe.
	 * Envio de un mensaje con "attachments" con URL mal.
	 * Envio de un mensaje a un email que no cumple el @Email.
	 * Envio de un mensaje con todos los campos en blanco.
	 * Envio de un mensaje sin estar autenticado.
	 * Envio de un mensaje con HTML en alg�n campo.
	 */
	@Test
	public void driverSave() {

		final Object testingData[][] = {

			{
				"customer1", "fegu@gmail.com", "Prueba", "prueba", "", null
			}, {
				"customer", "fegu@gmail.com", "Prueba", "prueba", "", IllegalArgumentException.class
			}, {
				"customer1", "fegumal@gmail.com", "Prueba", "prueba", "", IllegalArgumentException.class

			}, {
				"customer1", "fegu@gmail.com", "Prueba", "prueba", "urlmal", IllegalArgumentException.class

			}, {
				"customer", "fegugmailcom", "Prueba", "prueba", "", IllegalArgumentException.class

			}, {
				"customer1", "", "", "", "", IllegalArgumentException.class
			}, {
				"customer1", "fegu@gmail.com", "<p>", "<b>", "", ConstraintViolationException.class

			}, {
				null, "fegu@gmail.com", "Prueba", "prueba", "", IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Class<?>) testingData[i][5]);

	}

	//Erase his or her messages
	/*
	 * Se comprueba en el siguiente orden:
	 * Eliminar correctamente un mensaje.
	 * Eliminar un mensaje sin estar autenticado.
	 * Eliminar un mensaje que no es tuyo.
	 * Eliminar un mensaje que no existe.
	 */
	@Test
	public void driverDelete() {

		final Object testingData[][] = {

			{
				"customer2", 37, null
			}, {
				null, 37, NullPointerException.class
			}, {
				"customer1", 37, NullPointerException.class
			}, {
				"customer1", 50, NullPointerException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDelete((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	//List the messages that he/she is got
	/*
	 * Se comprueba la lista en el siguiente orden:
	 * Acceder a la lista de mensajes de un usuario logueado
	 * Acceder a la lista de mensajes de un usuario no autenticado (null)
	 */
	@Test
	public void driverList() {

		final Object testingData[][] = {

			{
				"customer1", null
			}, {
				null, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateList((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	protected void templateEdit(final String username, final String recipient, final String title, final String text, final String attachments, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Message a = this.messageService.create();

			a.setRecipient(recipient);
			a.setAttachments(attachments);
			a.setTitle(title);
			a.setText(text);
			this.messageService.save(a);
			this.messageService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	protected void templateList(final String username, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Customer c = this.customerService.findByPrincipal();

			c.getMessagesIncoming();
			c.getMessagesOutgoing();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateDelete(final String username, final Integer messageId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Message a = this.messageService.findOne(messageId);
			this.messageService.deleteWR(a);
			this.messageService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
