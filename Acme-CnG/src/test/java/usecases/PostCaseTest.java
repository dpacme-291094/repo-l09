
package usecases;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ApplicationService;
import services.PostService;
import utilities.AbstractTest;
import domain.Application;
import domain.Post;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class PostCaseTest extends AbstractTest {

	@Autowired
	private PostService			postService;
	@Autowired
	private ApplicationService	appService;


	//Ban an offer or a request that he or she finds inapropiatte
	/*
	 * En el siguiente orden:
	 * Un admin banea una offer correctamente
	 * Un admin banea una request correctamente
	 * Un customer intenta banear una offer
	 * Alguien no logueado intenta banear una request
	 * Un admin intenta banear un Post que no existe
	 */
	@Test
	public void driverBanPost() {

		final Object testingData[][] = {

			{
				"admin", 41, null
			}, {
				"admin", 44, null
			}, {
				"customer1", 41, null
			//no falla??
			}, {
				null, 44, null
			//no falla??
			}, {
				"admin", 55, NullPointerException.class

			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateBanPost((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	protected void templateBanPost(final String username, final Integer postId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Post a = this.postService.findOne(postId);
			a.setBanned(true);
			this.postService.save(a);
			this.postService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	//Apply for a request or an offer
	/*
	 * En el siguiente orden:
	 * Un customer envia una peticion a una oferta correctamente (Correcto)
	 * Un customer envia una petici�n a una oferta que no existe. (Erroneo)
	 * Null intenta crear una petici�n (Erroneo)
	 * Admin intenta crear una petici�n (Erroneo)
	 */
	@Test
	public void driverApply() {

		final Object testingData[][] = {

			{
				"customer2", 41, null
			}, {
				"customer1", 40, NullPointerException.class

			}, {
				null, 44, IllegalArgumentException.class

			}, {
				"admin", 44, ConstraintViolationException.class
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateApply((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateApply(final String username, final Integer postId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Post a = this.postService.findOne(postId);
			final Application b = this.appService.create(a);
			this.appService.save(b);
			this.postService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
