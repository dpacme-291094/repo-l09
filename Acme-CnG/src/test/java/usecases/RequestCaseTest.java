
package usecases;

import java.util.Date;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.RequestService;
import utilities.AbstractTest;
import domain.Coordinates;
import domain.Request;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class RequestCaseTest extends AbstractTest {

	@Autowired
	private RequestService	requestService;


	//Post a request in which he or she informs that 
	//he or she wishes to move from a place to another 
	//one and would like to find someone with whom he or she can share the trip.
	/*
	 * En el siguiente orden:
	 * Un customer crea una request correctamente (Correcto)
	 * Se intenta crear una request sin estar autenticado (Erroneo)
	 * Un customer intenta crear una request con los datos en blanco (Erroneo)
	 * Un administrador intenta crear una request (Erroneo)
	 * Request con el numero de asietos menor que el intervalo (Erroneo)
	 * Request con el numero de asietos mayor que el intervalo (Erroneo)
	 */
	@Test
	public void driverCreateRequest() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {

			{
				"customer2", "title", "description", "address", "address", null, null, new Date(2017, 5, 03, 15, 30), 2, null
			}, {
				"customer2", "title", "description", "address", "address", null, null, new Date(2017, 5, 03, 15, 30), -1, ConstraintViolationException.class
			}, {
				"customer2", "title", "description", "address", "address", null, null, new Date(2017, 5, 03, 15, 30), 6, ConstraintViolationException.class
			}, {
				null, "title", "description", "address", "address", null, null, new Date(2017, 5, 03, 15, 30), 2, IllegalArgumentException.class
			}, {
				"customer2", "", "", "", "", null, null, new Date(2017, 5, 03, 15, 30), 6, ConstraintViolationException.class
			}, {
				"admin", "title", "description", "address", "address", null, null, new Date(2017, 5, 03, 15, 30), 2, ConstraintViolationException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCreateRequest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Coordinates) testingData[i][5], (Coordinates) testingData[i][6],
				(Date) testingData[i][7], (Integer) testingData[i][8], (Class<?>) testingData[i][9]);

	}

	//Search for requests using a single keyword that must appear somewhere in their title
	//description or places.
	/*
	 * En el siguiente orden:
	 * Un customer busca una palabra correcta.
	 * Se intenta buscar null (Erroneo)
	 * Un administador hace una b�squeda. (Erroneo)
	 */
	@Test
	public void driverFindRequest() {

		final Object testingData[][] = {

			{
				"customer2", "trip", null
			}, {
				"customer2", null, null
			//comprobar
			}, {
				"admin", "trip", null
			//comprobar
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateFindRequest((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateCreateRequest(final String username, final String title, final String description, final String addressOrigin, final String addressDestination, final Coordinates coordori, final Coordinates coordes, final Date moment,
		final Integer places, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Request a = this.requestService.create();
			a.setAddressDestination(addressDestination);
			a.setAddressOrigin(addressOrigin);
			a.setCoordinatesDestination(coordes);
			a.setCoordinatesOrigin(coordori);
			a.setTitle(title);
			a.setPlaces(places);
			a.setDescription(description);
			a.setMoment(moment);
			this.requestService.save(a);
			this.requestService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateFindRequest(final String username, final String keyword, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			Assert.notNull(this.requestService.findRequestByTitleDescritionOrPlacesNotBanned(keyword));
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
