
package usecases;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.AdministratorService;
import services.ConfigurationService;
import utilities.AbstractTest;
import domain.Configuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class ConfigurationCaseTest extends AbstractTest {

	@Autowired
	private ConfigurationService	configService;
	@Autowired
	private AdministratorService	adminService;


	//Edit the banner
	/*
	 * En el siguiente orden:
	 * Editar el banner correctamente
	 * Un customer intenta editar un banner
	 * Alguien sin loguear intenta editar un banner
	 * El admin edita un banner con URL mal
	 * El admin edita un banner null
	 * Un usuario que no existe intenta editar un banner
	 */
	@Test
	public void driverEdit() {

		final Object testingData[][] = {

			{
				"admin", "https://www.google.es/", null
			}, {
				"customer1", "https://www.google.es/", null

			}, {
				null, "https://www.google.es/", IllegalArgumentException.class

			}, {
				"admin", "oogle.es", ConstraintViolationException.class

			}, {
				"admin", null, ConstraintViolationException.class
			}, {
				"administratir", "www.google.es", IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.template((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	protected void template(final String username, final String banner, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			this.adminService.findByPrincipal();
			final Configuration a = this.configService.findAll();
			a.setBanner(banner);
			this.configService.save(a);
			this.configService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
