
package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import services.CustomerService;
import utilities.AbstractTest;
import domain.Customer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class CustomerCaseTest extends AbstractTest {

	@Autowired
	private CustomerService	customerService;


	//	@Autowired
	//	private BindingResult	bindingResult;

	//Register as a customer
	/*
	 * En el siguiente orden:
	 * Registrarse correctamente.
	 * Las contrase�as no coinciden.
	 * Usuario que ya existe.
	 * Email no v�lido.
	 * Email que ya existe.
	 */
	@Test
	public void driverRegisterCustomer() {

		final Object testingData[][] = {

			{
				"customer4", "pass", "pass", "name", "surname", "email@gmail.com", "626626626", null
			}, {
				"customer4", "pass", "passwrong", "name", "surname", "email@gmail.com", "626626626", IllegalArgumentException.class
			}, {
				"customer1", "pass", "pass", "name", "surname", "email@gmail.com", "626626626", DataIntegrityViolationException.class

			}, {
				"customer5", "pass", "pass", "name", "surname", "emailnovalido", "626626626", DataIntegrityViolationException.class

			}, {
				"customer6", "pass", "pass", "name", "surname", "Mife@gmail.com", "626626626", DataIntegrityViolationException.class

			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateRegister((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(Class<?>) testingData[i][7]);

	}

	protected void templateRegister(final String username, final String password, final String repeatPassword, final String name, final String surname, final String email, final String phone, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			final Customer c = this.customerService.create();
			//			final FormCustomer fc = this.customerService.createForm();
			//			this.customerService.reconstruct(fc, this.bindingResult);

			final UserAccount ua = new UserAccount();
			ua.setUsername(username);
			ua.setPassword(password);
			final Authority aut = new Authority();
			aut.setAuthority("CUSTOMER");
			ua.addAuthority(aut);
			c.setUserAccount(ua);
			Assert.isTrue(password == repeatPassword);
			c.setEmail(email);
			c.setName(name);
			c.setSurname(surname);
			c.setPhone(phone);
			this.customerService.save(c);
			this.customerService.flush();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
