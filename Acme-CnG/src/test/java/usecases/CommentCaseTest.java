
package usecases;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ActorService;
import services.AdministratorService;
import services.CommentService;
import services.PostService;
import utilities.AbstractTest;
import domain.Actor;
import domain.Comment;
import domain.Post;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class CommentCaseTest extends AbstractTest {

	@Autowired
	private CommentService			commentService;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private AdministratorService	adminService;
	@Autowired
	private PostService				postService;


	//Post a comment on a post
	/*
	 * En el siguiente orden:
	 * Un admin comenta a un customer (correcto)
	 * Un admin comenta a un customer con 7 estrellas (erroneo)
	 * Un admin comenta a un customer con todo en blanco (erroneo)
	 * Alguien no logueado intenta comentar (erroneo)
	 */
	@Test
	public void driverPostCommentOnActor() {

		final Object testingData[][] = {

			{
				"admin", "title", "text", 2, 34, null
			}, {
				"admin", "title", "text", 7, 34, ConstraintViolationException.class
			}, {
				"admin", "", "", 7, 34, ConstraintViolationException.class
			}, {
				null, "text", "text", 5, 34, ConstraintViolationException.class
			//deberia petar
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templatePostCommentOnActor((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Integer) testingData[i][3], (Integer) testingData[i][4], (Class<?>) testingData[i][5]);

	}

	//Post a comment on an post
	/*
	 * En el siguiente orden:
	 * Un admin comenta un post (correcto)
	 * Un admin comenta un post con 7 estrellas (erroneo)
	 * Un admin comenta un post con todo en blanco (erroneo)
	 * Alguien no logueado intenta comentar (erroneo)
	 */
	@Test
	public void driverPostCommentOnPost() {

		final Object testingData[][] = {

			{
				"admin", "title", "text", 2, 41, null
			}, {
				"admin", "title", "text", 7, 41, ConstraintViolationException.class
			}, {
				"admin", "", "", 7, 41, ConstraintViolationException.class
			}, {
				null, "text", "text", 5, 41, ConstraintViolationException.class
			//deberia petar
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templatePostCommentOnPost((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Integer) testingData[i][3], (Integer) testingData[i][4], (Class<?>) testingData[i][5]);

	}

	//Ban an comment that he or she finds inapropiatte
	/*
	 * En el siguiente orden:
	 * Un admin banea una comment correctamente
	 * Un customer intenta banear un comment
	 * Un admin intenta banear un comment que no existe
	 */
	@Test
	public void driverBanComment() {

		final Object testingData[][] = {

			{
				"admin", 47, null
			}, {
				"customer1", 47, null
			//comprobar
			}, {
				null, 47, IllegalArgumentException.class

			}, {
				"admin", 55, NullPointerException.class

			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateBanComment((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	protected void templateBanComment(final String username, final Integer commentId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			this.adminService.findByPrincipal();
			final Comment a = this.commentService.findOne(commentId);
			a.setBanned(true);
			this.commentService.save(a);
			this.commentService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
	protected void templatePostCommentOnActor(final String username, final String title, final String text, final Integer stars, final Integer actorId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Comment a = this.commentService.create(0, 'a');
			final Actor b = this.actorService.findOne(actorId);
			a.setText(text);
			a.setTitle(title);
			a.setStars(stars);
			this.commentService.save(a);
			this.commentService.flush();
			b.addComment(a);
			this.actorService.save(b);
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
	protected void templatePostCommentOnPost(final String username, final String title, final String text, final Integer stars, final Integer postId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Comment a = this.commentService.create(0, 'p');
			final Post b = this.postService.findOne(postId);
			a.setText(text);
			a.setTitle(title);
			a.setStars(stars);
			this.commentService.save(a);
			this.commentService.flush();
			b.addComment(a);
			this.postService.save(b);
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
